<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Artlook API communication settings
    |--------------------------------------------------------------------------
    |
    */
    'api' => [
        'url' => env('APP_API_URL', 'https://api.artlookonline.com'),
        'endpoints' => [
            'user' => [
                'credentials' => [
                    'path' => '/credentials'
                ],
                'checkurl' => [
                    'path' => '/urlcheck/{host}'
                ],
                'settings' => [
                    'path' => '/prefs/{user_space}'
                ],
                'index' => [
                    'path' => '/home/{user_space}'
                ],
                'biography' => [
                    'path' => '/bio/{user_space}'
                ],
                'artists' => [
                    'path' => '/artists/{user_space}'
                ],
                'artist' => [
                    'path' => '/artist/{id}/{user_space}'
                ],
                'exhibitions' => [
                    'path' => '/exhibitions/{user_space}'
                ],
                'exhibition' => [
                    'path' => '/exhibition/{id}/{user_space}'
                ],
                'galleries' => [
                    'path' => '/galleries/{user_space}'
                ],
                'gallery' => [
                    'path' => '/gallery/{id}/{user_space}'
                ],
                'page' => [
                    'path' => '/webpage/{id}/{user_space}'
                ],
                'item' => [
                    'path' => '/artwork/{id}/{user_space}'
                ],
                'groups' => [
                    'path' => '/groups/{user_space}'
                ],
                'categories' => [
                    'path' => '/categorieslisting/{user_space}'
                ]
            ]
        ]
    ],
    /*
    |--------------------------------------------------------------------------
    | Artlook settings
    |--------------------------------------------------------------------------
    |
    */
    'settings' => [
        'main_domain' => env('APP_MAIN_DOMAIN', 'artlook-client-laravel.local'),
    ],
    /*
    |--------------------------------------------------------------------------
    | Artlook images settings
    |--------------------------------------------------------------------------
    |
    */
    'images' => [
        'images_server' => env('APP_IMAGES_SERVER', 'https://hello.artlookonline.com/images'),
        'image_cache' => env('APP_IMAGES_CACHE', 'https://hello.artlookonline.com/imagecache'),
    ],
    /*
    |--------------------------------------------------------------------------
    | Artlook email settings
    |--------------------------------------------------------------------------
    |
    */
    'email' => [
        'contact_form' => [
            'sender_email' => env('APP_USER_SPACE_DEFAULT_SENDER_EMAIL', 'no-reply@artlookonline.com'),
            'sender_name' => env('APP_USER_SPACE_DEFAULT_SENDER_NAME', 'Artlook Ltd.'),
            'subject' => env('APP_USER_SPACE_CONTACT_SUBJECT_EMAIL', 'Contact request from your Artlook website.'),
        ]
    ],
    /*
    |--------------------------------------------------------------------------
    | reCAPTCHA settings
    |--------------------------------------------------------------------------
    |
    */
    'recaptcha' => [
        'api_request' => env('APP_RECAPTCHA_API_REQUEST'),
        'api_secret' => env('APP_RECAPTCHA_API_SECRET'),
        'site_key' => env('APP_RECAPTCHA_API_SITE_KEY')
    ],
    /*
    |--------------------------------------------------------------------------
    | PayPal settings
    |--------------------------------------------------------------------------
    |
    */
    'paypal' => [
        'url' => env('APP_PAYPAL_URL', 'https://www.sandbox.paypal.com/cgi-bin/webscr'),
        'img' => env('APP_PAYPAL_PARAM_IMG', 'https://www.sandbox.paypal.com/en_GB/i/scr/pixel.gif'),
        'param_cmd' => env('APP_PAYPAL_PARAM_CMD', '_xclick'),
        'param_quantity' => env('APP_PAYPAL_PARAM_QUANTITY', '1'),
        'param_bn' => env('APP_PAYPAL_PARAM_BN', 'Artlook_BuyNow_WPS_GB'),
        'param_no_note' => env('APP_PAYPAL_PARAM_NO_NOTE', '0'),
        'param_no_shipping' => env('APP_PAYPAL_PARAM_NO_SHIPPING', '2'),
        'param_cn' => env('APP_PAYPAL_PARAM_CN', 'Add special instructions to the seller')
    ],
    /*
    |--------------------------------------------------------------------------
    | GA settings
    |--------------------------------------------------------------------------
    |
    */
    'ga' => [
        'default_tracking_code' => env('APP_GA_DEFAULT_TRACKING_CODE', 'UA-9652124-3'),
    ]
];
