<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title') - {{ $settings->orgname }}</title>
    <meta name="Title" content="@yield('title') - {{ $settings->orgname }}"/>
    @if (isset($settings->web_descript))
        <meta name="description" content="{{ $settings->web_descript }}"/>
    @endif
    @if (isset($settings->web_key))
        <meta name="keywords" content="{{ $settings->web_key }}"/>
    @endif
{{--    <link href="{{ elixir('user_space/css/all.css') }}" rel="stylesheet">--}}
    <link href="/user_space/css/all.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


    @yield('recaptcha_script')
</head>
<body class="theme-{{ (isset($settings->web_template) and intval($settings->web_template) > 1) ? $settings->web_template : '1' }} typography-pack-{{ (isset($settings->web_font) and intval($settings->web_font) > 1) ? $settings->web_font : '1' }} color-pack-{{ (isset($settings->web_colours) and intval($settings->web_colours) > 1) ? $settings->web_colours : '1' }}">
<header>
    @yield('main_menu')
</header>
<main class="container">
    @yield('main')
</main>
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 text-left">
                <p>{{ $settings->orgname }}</p>
            </div>
            <div class="col-sm-6 text-right">
                <p>
                @if (isset($settings->orgphone))
                    {{ $settings->orgphone }}
                @endif
                @if (isset($settings->orgemail))
                    {{ isset($settings->orgphone) ? ' | ': '' }}<a href="mailto:{{ $settings->orgemail }}">{{ $settings->orgemail }}</a>
                @endif
                </p>
            </div>
        </div>
    </div>
</footer>
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="pswp__bg"></div>
    <div class="pswp__scroll-wrap">

        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
            <div class="pswp__top-bar">
                <div class="pswp__counter"></div>
                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                <button class="pswp__button pswp__button--share" title="Share"></button>
                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                            <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div>
            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>
            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>
            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>
        </div>
    </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
{{--<script>window.jQuery || document.write('<script src="{{ elixir('user_space/js/jquery.js') }}"><\/script>')</script>--}}
<script>window.jQuery || document.write('<script src="/user_space/js/jquery.js"><\/script>')</script>
{{--<script src="{{ elixir('user_space/js/vendors.js') }}"></script>--}}
<script src="/user_space/js/vendors.js"></script>
<script src="/user_space/js/app.js"></script>
</body>
</html>