<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <meta name="Title" content="@yield('title')"/>
{{--    <link href="{{ elixir('user_space/css/all.css') }}" rel="stylesheet">--}}
    <link href="/user_space/css/all.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    @yield('recaptcha_script')
</head>
<body>
<header>
    header
</header>
<main class="container">
    @yield('main')
</main>
<footer class="footer">
     @yield('footer')
</footer>
<script src="//ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
{{--<script>window.jQuery || document.write('<script src="{{ elixir('user_space/js/jquery.js') }}"><\/script>')</script>--}}
<script>window.jQuery || document.write('<script src="/user_space/js/jquery.js"><\/script>')</script>
{{--<script src="{{ elixir('user_space/js/vendors.js') }}"></script>--}}
<script src="/user_space/js/vendors.js"></script>
{{--<script src="{{ elixir('user_space/js/app.js') }}"></script>--}}
<script src="/user_space/js/app.js"></script>
</body>
</html>
