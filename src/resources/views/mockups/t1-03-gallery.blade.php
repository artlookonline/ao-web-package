@extends('artlook::mockups.t1-00-layout_default')

@section('title', 'gallery_a')

@section('main')

    <!-- GROUPING LAYOUT//////////////////////////////////////////////////////   -->
    <section class="grouping grouping-layout grouping-layout-gallery-list">
        <div class="container">
            <article class="row">
                <div class="grouping-layout-title">
                    <h1><a href="#">Available galleries of work</a></h1>
                </div>
                <div class="grouping-gallery">
                    <ul class="row">
                        <li>
                            <a href="#">
                                <figure><span><img src="http://placehold.it/768x768"></span>
                                    <figcaption>Title</figcaption>
                                </figure>
                            </a>
                        </li>
                        <li class="col-sm-3">
                            <a href="#">
                                <figure><span><img src="http://placehold.it/768x768"></span>
                                    <figcaption>Title</figcaption>
                                </figure>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <figure><span><img src="http://placehold.it/768x768"></span>
                                    <figcaption>Title</figcaption>
                                </figure>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <figure><span><img src="http://placehold.it/768x768"></span>
                                    <figcaption>Title</figcaption>
                                </figure>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <figure><span><img src="http://placehold.it/768x768"></span>
                                    <figcaption>Title</figcaption>
                                </figure>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <figure><span><img src="http://placehold.it/768x768"></span>
                                    <figcaption>Title</figcaption>
                                </figure>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <figure><span><img src="http://placehold.it/768x768"></span>
                                    <figcaption>Title</figcaption>
                                </figure>
                            </a>
                        </li>
                        <li class="col-sm-3">
                            <a href="#">
                                <figure><span><img src="http://placehold.it/768x768"></span>
                                    <figcaption>Title</figcaption>
                                </figure>
                            </a>
                        </li>
                    </ul>
                </div>
            </article>
        </div>
    </section>
    <!-- END GROUPING //////////////////////////////////////////////////////   -->

@endsection
