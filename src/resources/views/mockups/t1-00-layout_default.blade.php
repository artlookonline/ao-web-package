<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <meta name="Title" content="@yield('title')"/>
{{--    <link href="{{ elixir('user_space/css/all.css') }}" rel="stylesheet">--}}
    <link href="/user_space/css/all.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    @yield('recaptcha_script')
    <link href="https://fonts.googleapis.com/css?family=Cabin:400,400i,700,700i|Fauna+One|Inconsolata:400,700|Lato:400,400i,900,900i|Montserrat:400,700|Muli:400,400i|PT+Sans:400,700|Playfair+Display:400,400i,700,700i|Roboto:400,400i,900,900i|Vollkorn:400,400i,700,700i|Satisfy" rel="stylesheet">
</head>
<body class="theme-1 typography-pack-6 color-pack-3">

<header class="navbar navbar-fixed-top">

    <div class="container">
        <div class="navbar-top">
            <div class="row">
                <div class="col-sm-6 hidden-xs text-left">
                    <ul>
                        <li><a href="#">+44 (0)1908 567398</a></li>
                        <li><a href="#">info@tradicionalartist.com</a></li>
                    </ul>
                </div>
                <div class="col-sm-6 hidden-xs text-right">
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Tradicional Artist</a>
        </div>
        <nav id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="#">Bio</a></li>
                <li><a href="#">Gallery</a></li>
                <li><a href="#">Exhibition</a></li>
                <li><a href="#">More about Picasso</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
        </nav>
    </div>
</header>
<main>
    <div class="wrapper">
        @yield('main')
    </div>
</main>
<footer class="footer text-center">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h6>Tradicional Artist</h6>
            </div>
            <div class="col-xs-12">
                <p>100 Milford Avenue, Stony Stratford, MK11 1HE, United Kingdom</p>
                <p>Tel: <a href="#">+44 (0)1908 567398</a></p>
                <p>Email: <a href="#">info@tradicionalartist.com</a></p>
            </div>
            <div class="col-xs-12 visible-xs">
                <ul>
                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                </ul>
            </div>
            <div class="col-xs-12 logo-footer">
{{--                <a href="#"><img src="{{ elixir('images/logo-artlook.svg') }}"/></a>--}}
                <a href="#"><img src="/images/logo-artlook.svg"/></a>
            </div>
        </div>
    </div>
</footer>
<script src="//ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="{{ elixir('user_space/js/jquery.js') }}"><\/script>')</script>
<script src="{{ elixir('user_space/js/vendors.js') }}"></script>
<script src="{{ elixir('user_space/js/app.js') }}"></script>
<script src="https://use.fontawesome.com/f5f8e0e63d.js"></script>
</body>
</html>
