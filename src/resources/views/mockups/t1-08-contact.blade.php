@extends('artlook::mockups.t1-00-layout_default')

@section('title', 'contact_a')

@section('main')

    <!-- GROUPING LAYOUT//////////////////////////////////////////////////////   -->
    <section class="grouping grouping-layout">
        <div class="container">
            <article class="row">
                <div class="grouping-layout-title">
                    <h1><a href="#">Contact</a></h1>
                </div>
                <div class="grouping-image">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d50860.94427586596!2d-3.6262911870430203!3d37.18100943189904!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd71fce62d32c27d%3A0x9258f79dd3600d72!2sGranada!5e0!3m2!1ses!2ses!4v1468517662280" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="grouping-content">
                    <div class="grouping-content-text">
                    <h5>To send a message or a request please use this form:</h5>
                    <form class="form-horizontal grouping-content-contact">
                        <div class="form-group">
                            <div class="control-input">
                                <input type="email" class="form-control" id="inputEmail3" placeholder="Your Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-input">
                                <input type="email" class="form-control" id="inputEmail3" placeholder="Your Email">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-textarea">
                                <textarea class="form-control" rows="3" placeholder="Your request"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-check">
                                <div class="checkbox"> <label> <input type="checkbox"> I have read and accept the <a href="#">privacy policy</a>.</label> </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-submit">
                                <button type="submit" class="btn btn-alt">Submit</button>
                            </div>
                        </div>
                    </form>

                    </div>
                </div>
            </article>
        </div>
    </section>
    <!-- END GROUPING //////////////////////////////////////////////////////   -->

@endsection
