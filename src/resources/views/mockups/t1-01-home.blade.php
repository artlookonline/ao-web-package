@extends('artlook::mockups.t1-00-layout_default')

@section('title', 'home_a')

@section('main')

    <!-- GROUPING HOME SLIDE//////////////////////////////////////////////////////   -->
    <section class="grouping grouping-home grouping-first">
        <div class="container">
            <article class="row">
                <div class="grouping-image">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                            <li data-target="#myCarousel" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner" role="listbox">
                            <div class="item active"><img src="http://placehold.it/1024x800"></div>
                            <div class="item"><img src="http://placehold.it/1024x800"></div>
                            <div class="item"><img src="http://placehold.it/1024x800"></div>
                        </div>
                        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
                <div class="grouping-content">
                    <div class="grouping-content-title">
                        <h1><a href="#">Pablo Picasso Forge</a></h1>
                    </div>
                    <div class="grouping-content-text">
                        <p>A prolific and tireless innovator of art forms, Pablo Picasso impacted the course of
                            20th-century art with unparalleled magnitude. Inspired by African and Iberian art and
                            developments in the world around him, Picasso contributed significantly to a number of
                            artistic movements, notably Cubism, Surrealism, Neoclassicism, and Expressionism.</p>
                    </div>
                    <div class="grouping-content-link">
                        <a href="#" class="btn btn-alt">View details</a>
                    </div>
                </div>
                <div class="grouping-gallery">
                    <ul class="row">
                        <li>
                            <a href="#">
                                <figure><span><img src="http://placehold.it/768x700 "></span><figcaption>Title</figcaption></figure>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <figure><span><img src="http://placehold.it/768x200"></span><figcaption>Title</figcaption></figure>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <figure><span><img src="http://placehold.it/768x2000"></span><figcaption>Title</figcaption></figure>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <figure><span><img src="http://placehold.it/768x600"></span><figcaption>Title</figcaption></figure>
                            </a>
                        </li>
                    </ul>
                </div>
            </article>
        </div>
    </section>
    <!-- END GROUPING //////////////////////////////////////////////////////   -->

    <!-- GROUPING NORMAL //////////////////////////////////////////////////////   -->
    <section class="grouping grouping-home">
        <div class="container">
            <article class="row">

                <div class="grouping-image">
                    <a href="#"><img src="http://placehold.it/370x370"></a>

                </div>
                <div class="grouping-content">
                    <div class="grouping-content-title">
                        <h2>100 page free book</h2>
                    </div>
                    <div class="grouping-content-text">
                        <p>Integer blandit placerat rutrum. Vivamus in tortor ex. Cras ullamcorper consectetur nulla, quis pulvinar libero viverra at. Phasellus hendrerit erat turpis, id efficitur nisl feugiat et. Duis non interdum justo. Integer convallis aliquam eleifend. Donec quis sem erat. Quisque mattis porttitor rhoncus.</p>
                    </div>
                    <div class="grouping-content-link">
                        <a href="#" class="btn btn-alt">View details</a>
                    </div>
                </div>
                <div class="grouping-gallery">
                    <ul class="row">
                        <li>
                            <a href="#">
                                <figure><span><img src="http://placehold.it/768x700 "></span><figcaption>Title</figcaption></figure>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <figure><span><img src="http://placehold.it/768x200"></span><figcaption>Title</figcaption></figure>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <figure><span><img src="http://placehold.it/768x2000"></span><figcaption>Title</figcaption></figure>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <figure><span><img src="http://placehold.it/768x600"></span><figcaption>Title</figcaption></figure>
                            </a>
                        </li>
                    </ul>
                </div>
            </article>
        </div>
    </section>
    <!-- END GROUPING //////////////////////////////////////////////////////   -->

    <!-- GROUPING ONLY image//////////////////////////////////////////////////////   -->
    <section class="grouping grouping-home grouping-only-image">
        <div class="container">
            <article class="row">
                <div class="grouping-image">
                    <img src="http://placehold.it/370x370">
                </div>
                <div class="grouping-content">
                    <div class="grouping-content-title">

                    </div>
                    <div class="grouping-content-text">

                    </div>
                    <div class="grouping-content-link">
                        <a href="#" class="btn btn-alt">View details</a>
                    </div>
                </div>
            </article>
        </div>
    </section>
    <!-- END GROUPING //////////////////////////////////////////////////////   -->

    <!-- GROUPING ONLY text//////////////////////////////////////////////////////   -->
    <section class="grouping grouping-home grouping-only-text">
        <div class="container">
            <article class="row">
                <div class="grouping-image">

                </div>
                <div class="grouping-content">
                    <div class="grouping-content-title">
                        <h2>100 page free book</h2>
                    </div>
                    <div class="grouping-content-text">
                        <p>Integer blandit placerat rutrum. Vivamus in tortor ex. Cras ullamcorper consectetur nulla,
                            quis pulvinar libero viverra at. Phasellus hendrerit erat turpis, id efficitur nisl feugiat
                            et. Duis non interdum justo. Integer convallis aliquam eleifend. Donec quis sem erat.
                            Quisque mattis porttitor rhoncus.</p>
                    </div>
                    <div class="grouping-content-link">
                        <a href="#" class="btn btn-alt">View details</a>
                    </div>
                </div>
            </article>
        </div>
    </section>
    <!-- END GROUPING //////////////////////////////////////////////////////   -->
    <!-- GROUPING ONLY gallery//////////////////////////////////////////////////////   -->
    <section class="grouping grouping-home grouping-only-text">
        <div class="container">
            <article class="row">
                <div class="grouping-image">

                </div>
                <div class="grouping-content">
                    <div class="grouping-content-title">

                    </div>
                    <div class="grouping-content-text">

                    </div>
                    <div class="grouping-content-link">

                    </div>
                </div>
                <div class="grouping-gallery">
                    <ul class="row">
                        <li>
                            <a href="#">
                                <figure><span><img src="http://placehold.it/768x700"></span><figcaption>Title</figcaption></figure>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <figure><span><img src="http://placehold.it/768x200"></span><figcaption>Title</figcaption></figure>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <figure><span><img src="http://placehold.it/768x2000"></span><figcaption>Title</figcaption></figure>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <figure><span><img src="http://placehold.it/768x600"></span><figcaption>Title</figcaption></figure>
                            </a>
                        </li>
                    </ul>
                </div>
            </article>
        </div>
    </section>
    <!-- END GROUPING //////////////////////////////////////////////////////   -->

@endsection
