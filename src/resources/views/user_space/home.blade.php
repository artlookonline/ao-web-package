@extends('artlook::user_space.layouts.default')

@inject('viewController', 'Artlook\Frontend\Http\Controllers\ViewController')

@push('body_class', ' home ')

@php
    $masonryClasses = ['container' => '', 'item' => ''];
@endphp

@if (isset($settings->web_template)
and (
intval($settings->web_template) === 2
or intval($settings->web_template) === 3
))
    @push('body_class', ' masonry ')
    @php
        $masonryClasses = ['container' => 'masonry-container', 'item' => 'masonry-item'];
    @endphp
@endif

@section('title', $title)

@section('main')
    @if (isset($main_item) and !empty($main_item))
        <!-- GROUPING HOME SLIDE//////////////////////////////////////////////////////   -->
        <section
                class="grouping grouping-home grouping-first {{ (isset($main_item->grouping_classes) and !empty($main_item->grouping_classes)) ? (implode(' ', array_filter($main_item->grouping_classes))) : '' }}">
            <div class="container">
                <article class="row">
                    @if (isset($main_item->all_images) and !empty($main_item->all_images))
                        <div class="grouping-image">
                            {!! $viewController->getCarousel(app('request'), $main_item, 'fullhd', 'homeMainCarousel') !!}
                        </div>
                    @endif
                    <div class="grouping-content">
                        <div class="grouping-content-title">
                            <h1 title="{{ htmlspecialchars($main_item->title) }}"> @if (isset($main_item->link_event) and !empty($main_item->link_event))
                                    <a href="{{ ViewHelper::getApplicationRouteByLinkEvent($main_item->link_event, app('request')) }}"
                                       title="{{ (isset($main_item->link_event->title)) ? htmlspecialchars($main_item->link_event->title) : htmlspecialchars($main_item->title) }}"> @endif {{ $main_item->title }} @if (isset($main_item->link_event) and !empty($main_item->link_event)) </a> @endif
                            </h1>
                        </div>
                        @if(isset($main_item->description) and !empty($main_item->description))
                            <div class="grouping-content-text">
                                {!! $main_item->description !!}
                            </div>
                        @endif
                        @if(isset($main_item->link_event) and !empty($main_item->link_event))
                            <div class="grouping-content-link">
                                <a href="{{ ViewHelper::getApplicationRouteByLinkEvent($main_item->link_event, app('request')) }}"
                                   title="{{ (isset($main_item->link_event->title)) ? htmlspecialchars($main_item->link_event->title) : htmlspecialchars($main_item->title) }}"
                                   class="btn btn-alt">View details</a>
                            </div>
                        @endif
                    </div>
                    @if (isset($main_item->items) and !empty($main_item->items))
                        <div class="grouping-gallery">
                            <ul class="row {{ $masonryClasses['container'] }}">
                                @foreach ($main_item->items as $currentItem)
                                    <li class="{{ $masonryClasses['item'] }}">
                                        <a href="{{ RouteHelper::route('user_space_item', [ 'user_space' => app('request')->user_space, 'id' => $currentItem->id, 'slug' => ViewHelper::getSlug($currentItem->title) ]) }}">
                                            <figure><span><img
                                                src="{{ ViewHelper::getAPICacheImagePath($currentItem->image, $settings->tenant_id, 'w768') }}"></span>
                                                <figcaption>@if($settings->type === 'gallery')<small class="item-artist-name">{{ $currentItem->artist }}</small>@endif{{ $currentItem->title }}</figcaption>
                                            </figure>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                @endif
                <!-- Masonry integration -->
                </article>
            </div>
        </section>
        <!-- END GROUPING //////////////////////////////////////////////////////   -->
    @endif
    @if (isset($subsidiaries_items) and !empty($subsidiaries_items))
        @foreach ($subsidiaries_items as $subsidiariesItem)
            <!-- GROUPING NORMAL //////////////////////////////////////////////////////   -->
            <section
                    class="grouping grouping-home {{ (isset($subsidiariesItem->grouping_classes) and !empty($subsidiariesItem->grouping_classes)) ? (implode(' ', array_filter($subsidiariesItem->grouping_classes))) : '' }}">
                <div class="container">
                    <article class="row">
                        @if (isset($subsidiariesItem->all_images) and !empty($subsidiariesItem->all_images))
                            <div class="grouping-image">
                                {!! $viewController->getCarousel(app('request'), $subsidiariesItem, 'slide', $subsidiariesItem->id.'Carousel') !!}
                            </div>
                        @endif
                        <div class="grouping-content">
                            <div class="grouping-content-title">
                                <h2 title="{{ htmlspecialchars($subsidiariesItem->title) }}"> @if (isset($subsidiariesItem->link_event) and !empty($subsidiariesItem->link_event))
                                        <a href="{{ ViewHelper::getApplicationRouteByLinkEvent($subsidiariesItem->link_event, app('request')) }}"
                                           title="{{ (isset($subsidiariesItem->link_event->title)) ? htmlspecialchars($subsidiariesItem->link_event->title) : htmlspecialchars($subsidiariesItem->title) }}"> @endif {{ $subsidiariesItem->title }} @if (isset($subsidiariesItem->link_event) and !empty($subsidiariesItem->link_event)) </a> @endif
                                </h2>
                            </div>
                            @if(isset($subsidiariesItem->description) and !empty($subsidiariesItem->description))
                                <div class="grouping-content-text">
                                    {!! $subsidiariesItem->description !!}
                                </div>
                            @endif
                            @if(isset($subsidiariesItem->link_event) and !empty($subsidiariesItem->link_event))
                                <div class="grouping-content-link">
                                    <a href="{{ ViewHelper::getApplicationRouteByLinkEvent($subsidiariesItem->link_event, app('request')) }}"
                                       title="{{ (isset($subsidiariesItem->link_event->title)) ? htmlspecialchars($subsidiariesItem->link_event->title) : htmlspecialchars($subsidiariesItem->title) }}"
                                       class="btn btn-alt">View details</a>
                                </div>
                            @endif
                        </div>
                        @if (isset($subsidiariesItem->items) and !empty($subsidiariesItem->items))
                            <div class="grouping-gallery">
                                <ul class="row {{ $masonryClasses['container'] }}">
                                    @foreach ($subsidiariesItem->items as $currentItem)
                                        <li class="{{ $masonryClasses['item'] }}">
                                            <a href="{{ RouteHelper::route('user_space_item', [ 'user_space' => app('request')->user_space, 'id' => $currentItem->id, 'slug' => ViewHelper::getSlug($currentItem->title) ]) }}">
                                                <figure><span><img
                                                                src="{{ ViewHelper::getAPICacheImagePath($currentItem->image, $settings->tenant_id, 'w768') }}"></span>
                                                    <figcaption>@if($settings->type === 'gallery')<small class="item-artist-name">{{ $currentItem->artist }}</small>@endif{{ $currentItem->title }}</figcaption>
                                                </figure>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </article>
                </div>
            </section>
            <!-- END GROUPING //////////////////////////////////////////////////////   -->
        @endforeach
    @endif
@endsection

@section('scripts')
    @if($settings->js)
    {!! $settings->js !!}
    @endif
@endsection