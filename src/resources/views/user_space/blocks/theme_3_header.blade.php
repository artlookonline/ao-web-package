<header class="navbar navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand"
               href="{{ RouteHelper::route('user_space_index', [ 'user_space' => app('request')->user_space ]) }}"
               title="{{ htmlspecialchars($settings->orgname) }}"> @if(isset($settings->logo) and !empty($settings->logo))
                    <img src="{{ $settings->logo }}" title="{{ htmlspecialchars($settings->orgname) }}"
                         alt="{{ htmlspecialchars($settings->orgname) }}"/>@else
                    <span>{{ $settings->orgname }}</span> @endif</a>
            @if($settings->orgstrap)
                <br>
                <p class="navbar-brand" style="font-size:90%">{{ $settings->orgstrap }}</p>
            @endif
        </div>
        <nav id="navbar" class="collapse navbar-collapse">
            @if (isset($settings->menu))
                @inject('viewController', 'Artlook\Frontend\Http\Controllers\ViewController')
                {!! $viewController->getMainMenu(app('request')) !!}
            @endif
            <div class="navbar-top">
                <div class="row">
                    @if (isset($settings->orgphone) or isset($settings->orgemail))
                        <div class="col-sm-6 hidden-xs text-left">
                            <ul>
                                @if (isset($settings->orgphone))
                                    <li>
                                        <a href="tel:{{ str_replace(' ', '', $settings->orgphone) }}">{{ $settings->orgphone }}</a>
                                    </li>
                                @endif
                                @if (isset($settings->orgemail))
                                    <li>
                                        <a href="mailto:{{ $settings->orgemail }}">{{ $settings->orgemail }}</a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    @endif
                    <div class="{{ (isset($settings->orgphone) or isset($settings->orgemail)) ? 'col-sm-6' : 'col-sm-12' }} hidden-xs text-right">
                        @include('artlook::user_space.shared.social_links')
                        @if(isset($settings->web_snipcart))
                        &nbsp;&nbsp;&nbsp;
                        <button class="snipcart-customer-signin" style="background: none!important;border: none;padding: 0!important;">
                            Your account
                        </button> |
                        <span class="snipcart-checkout">
                        <i class="fa fa-shopping-basket"></i>
                        <span class="badge snipcart-items-count"></span>
                        @endif
                    </span>
                    </div>
                </div>
            </div>
        </nav>
        <!-- SNIPCART -->
        @if(isset($settings->web_snipcart))
        <div hidden id="snipcart"
             data-api-key="OWJhZDZmMmUtZGQwMS00MTIxLThjMjQtMjhiZDE5Njc1ODM2NjM3NTgyNDA5MjMzMjc1ODAw"
             data-config-add-product-behavior="none"
             data-currency="GBP"
             data-config-modal-style="side">
        </div>
        @endif
        <!-- SNIPCART ENDS -->
    </div>
</header>
