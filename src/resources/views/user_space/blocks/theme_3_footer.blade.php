<footer class="footer text-center">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h6 title="{{ htmlspecialchars($settings->orgname, ENT_QUOTES) }}">{{ $settings->orgname }}</h6>
            </div>
            <div class="col-xs-12">
                @if (isset($settings->orgaddress))
                    <p>{{ $settings->orgaddress }}</p>
                @endif
                @if (isset($settings->orgphone))
                    <p>Tel: <a href="tel:{{ str_replace(' ', '', $settings->orgphone) }}">{{ $settings->orgphone }}</a>
                    </p>
                @endif
                @if (isset($settings->orgemail))
                    <p>Email: <a href="mailto:{{ $settings->orgemail }}">{{ $settings->orgemail }}</a></p>
                @endif
            </div>
            <div class="col-xs-12 visible-xs">
                <!-- Social MOD
                <ul>
                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                </ul>
                 -->
            </div>
            <div class="col-xs-12 logo-footer">
                <a href="https://www.artlooksoftware.com"
                   target="_blank"
                   title="Artlook Software - Inventory and website management for art galleries and artists">
                    <svg version="1.0" xmlns="http://www.w3.org/2000/svg"
                         width="439.000000pt" height="70.000000pt" viewBox="0 0 439.000000 70.000000"
                         preserveAspectRatio="xMidYMid meet">

                        <g transform="translate(0.000000,70.000000) scale(0.100000,-0.100000)"
                           fill="#000000" stroke="none">
                            <path d="M3940 350 l0 -350 70 0 70 0 0 69 c0 105 14 107 91 11 l64 -80 77 0
c43 0 78 3 78 6 0 4 -43 58 -97 121 l-96 114 84 85 84 84 -87 0 -86 0 -56 -62
-56 -61 0 206 0 207 -70 0 -70 0 0 -350z"/>
                            <path d="M162 342 c-89 -176 -162 -324 -162 -330 0 -8 24 -12 73 -12 l72 0
107 213 c90 178 208 429 208 443 0 2 -30 4 -67 4 l-68 0 -163 -318z"/>
                            <path d="M1252 408 c-156 -304 -202 -396 -202 -403 0 -3 33 -5 74 -5 l74 0 98
190 c54 104 102 186 106 182 4 -4 45 -88 92 -187 l85 -180 76 -3 c50 -2 76 1
73 8 -1 5 -70 154 -153 330 -129 275 -153 320 -172 320 -18 0 -42 -39 -151
-252z"/>
                            <path d="M2550 330 l0 -330 190 0 190 0 0 70 0 70 -120 0 -120 0 0 260 0 260
-70 0 -70 0 0 -330z"/>
                            <path d="M2250 485 l0 -75 -40 0 -40 0 0 -60 0 -60 40 0 40 0 0 -145 0 -145
60 0 60 0 0 145 0 145 50 0 50 0 0 60 0 60 -50 0 -50 0 0 75 0 75 -60 0 -60 0
0 -75z"/>
                            <path d="M1966 410 c-15 -5 -36 -16 -46 -25 -17 -15 -19 -15 -25 4 -5 18 -15
21 -56 21 l-49 0 0 -205 0 -205 60 0 60 0 0 110 c0 80 4 119 15 140 20 39 68
63 106 55 27 -6 31 -4 54 47 23 49 23 53 7 60 -23 9 -91 8 -126 -2z"/>
                            <path d="M3095 408 c-87 -30 -139 -107 -139 -203 0 -87 46 -160 121 -191 49
-21 166 -18 211 5 129 66 152 242 45 343 -56 53 -160 73 -238 46z m134 -114
c64 -46 58 -143 -12 -178 -33 -16 -42 -16 -69 -5 -36 15 -68 63 -68 103 0 39
58 96 97 96 16 0 39 -7 52 -16z"/>
                            <path d="M3575 401 c-130 -60 -167 -237 -72 -339 45 -48 93 -64 182 -60 70 2
88 7 122 31 113 78 119 260 11 340 -68 50 -170 62 -243 28z m138 -102 c31 -14
57 -59 57 -98 0 -47 -48 -95 -95 -95 -30 0 -44 7 -66 32 -47 52 -38 126 19
156 33 18 53 19 85 5z"/>
                            <path d="M450 70 l0 -70 190 0 190 0 0 70 0 70 -190 0 -190 0 0 -70z"/>
                        </g>
                    </svg>
                </a>
            </div>
        </div>
    </div>
</footer>
