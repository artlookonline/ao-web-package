@extends('artlook::user_space.layouts.default')

@inject('viewController', 'Artlook\Frontend\Http\Controllers\ViewController')

@push('body_class', ' page ')

@php
    $masonryClasses = ['container' => '', 'item' => ''];
@endphp

@if (isset($settings->web_template)
and (
intval($settings->web_template) === 2
or intval($settings->web_template) === 3
))
    @push('body_class', ' masonry ')
    @php
        $masonryClasses = ['container' => 'masonry-container', 'item' => 'masonry-item'];
    @endphp
@endif

@section('title', $title)

@section('main')
    @if (isset($main_item) and !empty($main_item))
        <!-- GROUPING HOME SLIDE//////////////////////////////////////////////////////   -->
        <section class="grouping grouping-layout grouping-layout-page {{ (isset($main_item->grouping_classes) and !empty($main_item->grouping_classes)) ? (implode(' ', array_filter($main_item->grouping_classes))) : '' }}">
            <div class="container">
                <article class="row">
                    @if (isset($main_item->all_images) and !empty($main_item->all_images))
                        <div class="grouping-image">
                            {!! $viewController->getCarousel(app('request'), $main_item, 'slide', $main_item->id.'Carousel') !!}
                        </div>
                    @endif
                    <div class="grouping-content">
                        <div class="grouping-content-title">
                            <h1 title="{{ htmlspecialchars($main_item->title) }}"> @if (isset($main_item->link_event) and !empty($main_item->link_event))
                                    <a href="{{ ViewHelper::getApplicationRouteByLinkEvent($main_item->link_event, app('request')) }}"
                                       title="{{ (isset($main_item->link_event->title)) ? htmlspecialchars($main_item->link_event->title) : htmlspecialchars($main_item->title) }}"> @endif {{ $main_item->title }} @if (isset($main_item->link_event) and !empty($main_item->link_event)) </a> @endif
                            </h1>
                        </div>
                        @if(isset($main_item->description) and !empty($main_item->description))
                            <div class="grouping-content-text">
                                {!! $main_item->description !!}
                            </div>
                        @endif
                        @if(isset($main_item->link_event) and !empty($main_item->link_event))
                            <div class="grouping-content-link">
                                <a href="{{ ViewHelper::getApplicationRouteByLinkEvent($main_item->link_event, app('request')) }}"
                                   title="{{ (isset($main_item->link_event->title)) ? htmlspecialchars($main_item->link_event->title) : htmlspecialchars($main_item->title) }}"
                                   class="btn btn-alt">View details</a>
                            </div>
                        @endif
                    </div>
                    @if (isset($main_item->items) and !empty($main_item->items))
                        <div class="grouping-gallery">
                            <ul class="row {{ $masonryClasses['container'] }}">
                                @if (isset($main_item->artist_indexed) and $main_item->artist_indexed === 1
                                and isset($main_item->artists) and !empty($main_item->artists))
                                    @foreach ($main_item->artists as $artist_id => $artist_name)
                                        <li class="{{ $masonryClasses['item'] }}">
                                            <a href="{{ RouteHelper::route('user_space_page_artist', [ 'user_space' => app('request')->user_space, 'page_id' => $main_item->id, 'page_slug' => ViewHelper::getSlug($main_item->title), 'artist_slug' => ViewHelper::getSlug($artist_name), 'artist_id' => $artist_id ]) }}">
                                                <figure>
                                                    <span>
                                                        <img src="{{ ViewHelper::getArtistAPICacheImage($artist_id, $settings->tenant_id, 'w768') }}">
                                                    </span>
                                                    <figcaption>
                                                        {{ $artist_name }}
                                                    </figcaption>
                                                </figure>
                                            </a>
                                        </li>
                                    @endforeach
                                @else
                                    @foreach ($main_item->items as $currentItem)
                                        <li class="{{ $masonryClasses['item'] }}">
                                            <a href="{{ RouteHelper::route('user_space_pages_item', [ 'user_space' => app('request')->user_space, 'page_id' => $main_item->id, 'item_id' => $currentItem->id, 'item_slug' => ViewHelper::getSlug($currentItem->title) ]) }}">
                                                <figure>
                                                    <span>
                                                        <img src="{{ ViewHelper::getAPICacheImagePath($currentItem->image, $settings->tenant_id, 'w768') }}">
                                                    </span>
                                                    <figcaption>
                                                        @if($settings->type === 'gallery')<small
                                                                class="item-artist-name">{{ $currentItem->artist }}</small>
                                                        @endif
                                                        {{ $currentItem->title }}<br>
                                                            <small>
                                                            @include('artlook::user_space.shared.thumb_status_display')
                                                            </small>
                                                    </figcaption>
                                                </figure>
                                            </a>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    @endif
                </article>
            </div>
        </section>
        <!-- END GROUPING //////////////////////////////////////////////////////   -->
    @endif
@endsection
