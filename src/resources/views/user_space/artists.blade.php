@extends('artlook::user_space.layouts.default')

@push('body_class', ' artists ')

@php
    $masonryClasses = ['container' => '', 'item' => ''];
@endphp

@if (isset($settings->web_template)
and (
intval($settings->web_template) === 2
or intval($settings->web_template) === 3
))
    @push('body_class', ' masonry ')
    @php
        $masonryClasses = ['container' => 'masonry-container', 'item' => 'masonry-item'];
    @endphp
@endif

@section('title', $title)

@section('main')
    @if ((isset($main_item) and !empty($main_item))
        or (isset($subsidiaries_items) and !empty($subsidiaries_items)))
        <!-- GROUPING LAYOUT//////////////////////////////////////////////////////   -->
        <section class="grouping grouping-layout grouping-layout-gallery-list">
            <div class="container">
                <article class="row">
                    <div class="grouping-layout-title">
                        <h1 title="{{ $settings->web_artists }}">{{ $settings->web_artists }}</h1>
                    </div>
                    <div class="grouping-gallery">
                        <ul class="row {{ $masonryClasses['container'] }}">
                            @if (isset($main_item) and !empty($main_item))
                                <li class="{{ $masonryClasses['item'] }}">
                                    <a href="{{ RouteHelper::route('user_space_artist', [ 'user_space' => app('request')->user_space, 'id' => $main_item->id, 'slug' => ViewHelper::getSlug($main_item->name) ]) }}">
                                        <figure><span>@if(isset($main_item->profileimage) and !empty($main_item->profileimage))<img
                                                        src="{{ ViewHelper::getAPICacheImagePath($main_item->profileimage, $settings->tenant_id, 'x768') }}">@endif</span>
                                            <figcaption>{{ $main_item->name }}</figcaption>
                                        </figure>
                                    </a>
                                </li>
                            @endif
                            @if (isset($subsidiaries_items) and !empty($subsidiaries_items))
                                @foreach($subsidiaries_items as $item)
                                    <li class="{{ $masonryClasses['item'] }}">
                                        <a href="{{ RouteHelper::route('user_space_artist', [ 'user_space' => app('request')->user_space, 'id' => $item->id, 'slug' => ViewHelper::getSlug($item->name) ]) }}">
                                            <figure><span>@if(isset($item->profileimage) and !empty($item->profileimage))<img
                                                            src="{{ ViewHelper::getAPICacheImagePath($item->profileimage, $settings->tenant_id, 'x768') }}">@endif</span>
                                                <figcaption>{{ $item->name }}</figcaption>
                                            </figure>
                                        </a>
                                    </li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </article>
            </div>
        </section>
        <!-- END GROUPING //////////////////////////////////////////////////////   -->
    @endif
@endsection
