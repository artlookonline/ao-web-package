@extends('artlook::user_space.layouts.default')

@push('body_class', ' contact ')

@section('title', $title)

@section('head_og_type')
    <meta property="og:type" content="business.business"/>
@endsection

@section('styles')
    <style>
        .zaney{
            opacity: 0;
            position: absolute;
            top: 0;
            left: 0;
            height: 0;
            width: 0;
            z-index: -1;
        }
    </style>
@endsection

@section('recaptcha_script')
    <?php
    /**
     * ToDo
     *
     * ZCL@20170830
     *
     * Añadir la clave del
     * sitio de reCaptcha
     * en el fichero de configuración.
     */
    ?>
    <script src='https://www.google.com/recaptcha/api.js?onload=onloadRecaptchaCallback&render=explicit&hl={{ $settings->web_language }}'></script>
    <script>
        var grecaptchaWidget;
        var onloadRecaptchaCallback = function () {
            window.grecaptchaWidget = grecaptcha.render('reCaptchaContainer', {
                'sitekey': '{{ Config::get('artlook.recaptcha.site_key') }}',
                'size': 'normal'
            });
        }
    </script>
@endsection

@section('main')
    <!-- GROUPING LAYOUT//////////////////////////////////////////////////////   -->
    <section
            class="grouping grouping-layout grouping-layout-page {{ (!isset($settings->web_map) or empty($settings->web_map)) ? 'grouping-only-text' : '' }}">
        <div class="container">
            <article class="row">
                <div class="grouping-layout-title">
                    <h1 title="{{ htmlspecialchars(trans('artlook::artlook.' . $title)) }}">{{ trans('artlook::artlook.' . $title) }}</h1>
                </div>
                @if(isset($settings->web_map)
                    and !is_null($settings->web_map)
                    and strlen($settings->web_map))
                    <div class="grouping-image">
                        {!! $settings->web_map !!}
                    </div>
                @endif
                <div class="grouping-content">
                    <div class="grouping-content-text">
                        <h5>@lang('artlook::artlook.toSendUseForm')</h5>
                        <form class="form-validate form-ajax form-horizontal grouping-content-contact"
                              method="POST"
                              action="{{ RouteHelper::route('user_space_contact_form', [ 'user_space' => app('request')->user_space])}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                            @if (app('request')->query->has('contact_type'))
                                <input type="hidden" name="contact_type"
                                       value="{{ app('request')->query->get('contact_type') }}"/>
                            @endif
                            @if (app('request')->query->has('item_id'))
                                <input type="hidden" name="item_id"
                                       value="{{ app('request')->query->get('item_id') }}"/>
                            @endif
                            @if (app('request')->query->has('item_title'))
                                <input type="hidden" name="item_title"
                                       value="{{ app('request')->query->get('item_title') }}"/>
                            @endif
                            <div class="form-group">
                                <div class="control-input">
                                    <input type="text" name="name" id="inputName" class="form-control"
                                           placeholder="@lang('artlook::artlook.yourName')"
                                           required="required"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="control-input">
                                    <input type="email" name="email" id="inputEmail" class="form-control"
                                           placeholder="@lang('artlook::artlook.yourEmail')"
                                           required="required"/>
                                    <input type="text" name="website" id="inputWebsite" class="zaney"
                                           autocomplete="off" placeholder="Website"/>
                                </div>
                            </div>

                            @if (app('request')->query->has('contact_type')
                            and (app('request')->query->get('contact_type') === 'item_more_info'))
                                <div class="form-group">
                                    <div class="control-input">
                                        <input type="text" name="subject" id="inputsubject"
                                               class="form-control read-only"
                                               placeholder="@lang('artlook::artlook.subject')"
                                               readonly="readonly"
                                               value='@lang('artlook::artlook.enquireAbout') "{{ app('request')->query->get('item_title') }}" ({{ app('request')->query->get('item_id') }})'/>
                                    </div>
                                </div>
                            @endif
                            <div class="form-group">
                                <div class="control-textarea">
                                    <textarea name="note" id="textareaNote" class="form-control" rows="3"
                                              placeholder="@lang('artlook::artlook.yourMessage')" required="required"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="control-check">
                                    <div id="reCaptchaContainer"></div>
                                    <input type="hidden" name="reCaptchaValidation" id="inputReCaptchaValidation"
                                           required="required"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="control-submit">
                                    <button type="submit" class="btn btn-alt" data-loading-text="@lang('artlook::artlook.sending')">@lang('artlook::artlook.submit')
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </article>
        </div>
    </section>
    <!-- END GROUPING //////////////////////////////////////////////////////   -->
@endsection
