<div id="{{ $carousel_id }}" class="carousel slide" data-ride="carousel">
    @if (count($carousel_images) > 1)
        <ol class="carousel-indicators">
            @foreach ($carousel_images as $carousel_image_index => $carousel_image)
                <li data-target="#{{ $carousel_id }}" data-slide-to="{{ $carousel_image_index }}"
                    class="{{ ($carousel_image_index === 0 ) ? 'active': '' }}"></li>
            @endforeach
        </ol>
    @endif
    <div class="carousel-inner" role="listbox">
        @foreach ($carousel_images as $carousel_image_index => $carousel_image)
            <div class="item {{ ($carousel_image_index === 0 ) ? 'active': '' }}">
                @if(isset($add_anchor_image_to_item) and $add_anchor_image_to_item === true and isset($large_images) and !empty($large_images))
                    <a href="{{ $large_images[$carousel_image_index] }}"
                       rel="gallery-{{ $carousel_item->id }}"
                       class="lightbox">
                @elseif(isset($carousel_item->link_event) and !empty($carousel_item->link_event) and count($carousel_images) === 1)
                        <a href="{{ ViewHelper::getApplicationRouteByLinkEvent($carousel_item->link_event, app('request')) }}"
                           title="{{ (isset($carousel_item->link_event->title)) ? htmlspecialchars($carousel_item->link_event->title) : htmlspecialchars($carousel_item->title) }}">
                @endif
                <img src="{{ $carousel_image }}" />
                @if((isset($add_anchor_image_to_item) and $add_anchor_image_to_item === true and isset($large_images) and !empty($large_images))
                    or (isset($carousel_item->link_event) and !empty($carousel_item->link_event) and count($carousel_images) === 1))
                    </a>
                @endif
            </div>
        @endforeach
    </div>
    @if (count($carousel_images) > 1)
        <a class="left carousel-control" href="#{{ $carousel_id }}" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#{{ $carousel_id }}" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    @endif
</div>