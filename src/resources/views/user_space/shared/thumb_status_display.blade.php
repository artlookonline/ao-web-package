@unless( !$currentItem->price || $currentItem->price === 'false'  || (isset($settings->web_status) && $settings->web_status === 'False'))
    @if( $currentItem->price )
        {{ $currentItem->symbol }}{{ $currentItem->price }}
    @elseif( isset($currentItem->status))
        {{ $currentItem->status }}
    @endif
@endunless