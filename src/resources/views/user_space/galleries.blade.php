@extends('artlook::user_space.layouts.default')

@push('body_class', ' galleries ')

@php
    $masonryClasses = ['container' => '', 'item' => ''];
@endphp

@if (isset($settings->web_template)
and (
intval($settings->web_template) === 2
or intval($settings->web_template) === 3
))
    @push('body_class', ' masonry ')
    @php
        $masonryClasses = ['container' => 'masonry-container', 'item' => 'masonry-item'];
    @endphp
@endif

@section('title', $title)

@section('main')
    @if ((isset($main_item) and !empty($main_item))
        or (isset($subsidiaries_items) and !empty($subsidiaries_items)))
        <!-- GROUPING LAYOUT//////////////////////////////////////////////////////   -->
        <section class="grouping grouping-layout grouping-layout-gallery-list">
            <div class="container">
                <article class="row">
                    <div class="grouping-layout-title">
                        <h1 title="{{ htmlspecialchars($settings->web_galleries) }}">{{ $settings->web_galleries }}</h1>
                    </div>
                    <div class="grouping-gallery">
                        <ul class="row {{ $masonryClasses['container'] }}">
                            @if (isset($main_item) and !empty($main_item))
                                <li class="{{ $masonryClasses['item'] }}">
                                    <a href="{{ RouteHelper::route('user_space_gallery', [ 'user_space' => app('request')->user_space, 'id' => $main_item->id, 'slug' => ViewHelper::getSlug($main_item->title) ]) }}">
                                        <figure><span>@if(isset($main_item->image) and !empty($main_item->image))<img
                                                        src="{{ ViewHelper::getAPICacheImagePath($main_item->image, $settings->tenant_id, 'x768') }}">@endif</span>
                                            <figcaption>{{ $main_item->title }}</figcaption>
                                        </figure>
                                    </a>
                                </li>
                            @endif
                            @if (isset($subsidiaries_items) and !empty($subsidiaries_items))
                                @foreach($subsidiaries_items as $item)
                                    <li class="{{ $masonryClasses['item'] }}">
                                        <a href="{{ RouteHelper::route('user_space_gallery', [ 'user_space' => app('request')->user_space, 'id' => $item->id, 'slug' => ViewHelper::getSlug($item->title) ]) }}">
                                            <figure><span><img
                                                            src="{{ ViewHelper::getAPICacheImagePath($item->image, $settings->tenant_id, 'x768') }}"></span>
                                                <figcaption>{{ $item->title }}</figcaption>
                                            </figure>
                                        </a>
                                    </li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </article>
            </div>
        </section>
        <!-- END GROUPING //////////////////////////////////////////////////////   -->
    @endif
@endsection
