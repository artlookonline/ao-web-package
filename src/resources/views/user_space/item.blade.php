@extends('artlook::user_space.layouts.default')

@inject('viewController', 'Artlook\Frontend\Http\Controllers\ViewController')

@push('body_class', ' item ')

@section('title', $title)

@if (isset($main_item) and !is_null($main_item))
    @section('head_og_image')
        <meta property="og:image" content="'.$main_item->image.'"/>
    @endsection
@endif

@section('main')
    @if (isset($main_item) and !is_null($main_item))

{{--        {!! dd( $settings->tenant_id ) !!}--}}

        <!-- GROUPING LAYOUT//////////////////////////////////////////////////////   -->
        <section class="grouping grouping-layout grouping-layout-item">
            <div class="container">
                <article class="row">
                    @if (isset($main_item->all_images) and !empty($main_item->all_images))
                        <div class="grouping-image">
                            {!! $viewController->getCarousel(app('request'), $main_item, 'w768', $main_item->id.'Carousel') !!}
                        </div>
                    @endif
                    <div class="grouping-content">
                        @if (Route::currentRouteName() === $domain_type.'::user_space_exhibition_item'
                                        or Route::currentRouteName() === $domain_type.'::user_space_gallery_item'
                                        or Route::currentRouteName() === $domain_type.'::user_space_artist_item'
                                        or Route::currentRouteName() === $domain_type.'::user_space_pages_item'
                                        or Route::currentRouteName() === $domain_type.'::user_space_page_artist_item'
                                        or Route::currentRouteName() === $domain_type.'::user_space_gallery_artist_item'
                                        or Route::currentRouteName() === $domain_type.'::user_space_exhibition_artist_item')
                            <div class="grouping-content-breadcrumbs">
                                <ul>
                                    @if (Route::currentRouteName() === $domain_type.'::user_space_exhibition_item')
                                        <li>
                                            <a href="{{ RouteHelper::route('user_space_exhibitions', [ 'user_space' => app('request')->user_space ]) }}">{{ $settings->web_exhibitions }}</a>
                                        </li>
                                        @if (isset($parent) and !empty($parent))
                                            <li>
                                                <a href="{{ RouteHelper::route('user_space_exhibition', [ 'user_space' => app('request')->user_space, 'id' => $parent->id, 'slug' => ViewHelper::getSlug($parent->title) ]) }}">{{ $parent->title }}</a>
                                            </li>
                                        @endif
                                    @elseif (Route::currentRouteName() === $domain_type.'::user_space_gallery_item')
                                        <li>
                                            <a href="{{ RouteHelper::route('user_space_galleries', [ 'user_space' => app('request')->user_space ]) }}">{{ $settings->web_galleries }}</a>
                                        </li>
                                        @if (isset($parent) and !empty($parent))
                                            <li>
                                                <a href="{{ RouteHelper::route('user_space_gallery', [ 'user_space' => app('request')->user_space, 'id' => $parent->id, 'slug' => ViewHelper::getSlug($parent->title) ]) }}">{{ $parent->title }}</a>
                                            </li>
                                        @endif
                                    @elseif (Route::currentRouteName() === $domain_type.'::user_space_artist_item')
                                        <li>
                                            <a href="{{ RouteHelper::route('user_space_artists', [ 'user_space' => app('request')->user_space ]) }}">{{ $settings->web_artists }}</a>
                                        </li>
                                        @if (isset($parent) and !empty($parent))
                                            <li>
                                                <a href="{{ RouteHelper::route('user_space_artist', [ 'user_space' => app('request')->user_space, 'id' => $parent->id, 'slug' => ViewHelper::getSlug($parent->full_name) ]) }}">{{ $parent->full_name }}</a>
                                            </li>
                                        @endif
                                    @elseif (Route::currentRouteName() === $domain_type.'::user_space_pages_item')
                                        @if (isset($parent) and !empty($parent))
                                            <li>
                                                <a href="{{ RouteHelper::route('user_space_page', [ 'user_space' => app('request')->user_space, 'id' => $parent->id, 'slug' => ViewHelper::getSlug($parent->title) ]) }}">{{ $parent->title }}</a>
                                            </li>
                                        @endif
                                    @elseif (Route::currentRouteName() === $domain_type.'::user_space_page_artist_item')
                                        @if (isset($parent) and !empty($parent))
                                            <li>
                                                <a href="{{ RouteHelper::route('user_space_page', [ 'user_space' => app('request')->user_space, 'id' => $parent->id, 'slug' => ViewHelper::getSlug($parent->title) ]) }}">{{ $parent->title }}</a>
                                            </li>
                                        @endif
                                        <li>
                                            <a href="{{ RouteHelper::route('user_space_page_artist', [ 'user_space' => app('request')->user_space, 'page_id' => $parent->id, 'page_slug' => ViewHelper::getSlug($parent->title), 'artist_slug' => ViewHelper::getSlug($main_item->artist) ,'artist_id' => $main_item->artistId ]) }}">{{ $main_item->artist }}</a>
                                        </li>
                                    @elseif (Route::currentRouteName() === $domain_type.'::user_space_gallery_artist_item')
                                        <li>
                                            <a href="{{ RouteHelper::route('user_space_galleries', [ 'user_space' => app('request')->user_space ]) }}">{{ ViewHelper::getMainMenuGalleryLabel(app('request')) }}</a>
                                        </li>
                                        @if (isset($parent) and !empty($parent))
                                            <li>
                                                <a href="{{ RouteHelper::route('user_space_gallery', [ 'user_space' => app('request')->user_space, 'id' => $parent->id, 'slug' => ViewHelper::getSlug($parent->title) ]) }}">{{ $parent->title }}</a>
                                            </li>
                                        @endif
                                        <li>
                                            <a href="{{ RouteHelper::route('user_space_gallery_artist', [ 'user_space' => app('request')->user_space, 'gallery_id' => $parent->id, 'gallery_slug' => ViewHelper::getSlug($parent->title), 'artist_slug' => ViewHelper::getSlug($main_item->artist) ,'artist_id' => $main_item->artistId ]) }}">{{ $main_item->artist }}</a>
                                        </li>
                                    @elseif (Route::currentRouteName() === $domain_type.'::user_space_exhibition_artist_item')
                                        <li>
                                            <a href="{{ RouteHelper::route('user_space_exhibitions', [ 'user_space' => app('request')->user_space ]) }}">{{ ViewHelper::getMainMenuExhibitionLabel(app('request')) }}</a>
                                        </li>
                                        @if (isset($parent) and !empty($parent))
                                            <li>
                                                <a href="{{ RouteHelper::route('user_space_exhibition', [ 'user_space' => app('request')->user_space, 'id' => $parent->id, 'slug' => ViewHelper::getSlug($parent->title) ]) }}">{{ $parent->title }}</a>
                                            </li>
                                        @endif
                                        <li>
                                            <a href="{{ RouteHelper::route('user_space_exhibition_artist', [ 'user_space' => app('request')->user_space, 'exhibition_id' => $parent->id, 'exhibition_slug' => ViewHelper::getSlug($parent->title), 'artist_slug' => ViewHelper::getSlug($main_item->artist) ,'artist_id' => $main_item->artistId ]) }}">{{ $main_item->artist }}</a>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        @endif
                        <div class="grouping-content-title">
                            <h1 title="@if($settings->type === 'gallery'){{ htmlspecialchars($main_item->artist) }} - @endif{{ htmlspecialchars($main_item->title) }}">@if($settings->type === 'gallery')<small class="item-artist-name">{{ $main_item->artist }}</small>@endif{{ $main_item->title }}</h1>
                        </div>
                        <div class="grouping-content-info">
                            <ul>
                                @if (isset($main_item->category) and !empty($main_item->category))
                                    <li>{{ $main_item->category }}</li>
                                @endif
                                @if (isset($main_item->medium) and !empty($main_item->medium))
                                    <li>{{ $main_item->medium }}</li>
                                @endif
                                @if (isset($main_item->dimensionsText) and !empty($main_item->dimensionsText))
                                    <li>{{ $main_item->dimensionsText }}</li>
                                @endif
                            </ul>
                            @php
                                $reference = [];
                            @endphp
                            @if(isset($main_item->id) and !empty($main_item->id))
                                @php
                                    $reference[] = $main_item->id;
                                @endphp
                            @endif
                            @if(isset($main_item->reference) and !empty($main_item->reference))
                                @php
                                    $reference[] = $main_item->reference;
                                @endphp
                            @endif
                            @if(!empty($reference))
                                <ul>
                                    <li>Ref: {{ join('/', $reference) }}</li>
                                </ul>
                            @endif
                        </div>
                        @if (isset($main_item->price) and !empty($main_item->price) and $main_item->price !== false)
                            <div class="grouping-content-price">
                                <h6>
                                    @if (is_numeric($main_item->base_price))
                                        @if ($main_item->currency !== 'EUR')
                                            <small>{{  $main_item->symbol }}</small>
                                        @endif
                                        {{ ViewHelper::currencyformat($main_item->base_price, $main_item->currency) }}
                                        @if ($main_item->currency === 'EUR')
                                            <small>{{  $main_item->symbol }}</small>
                                        @endif
                                    @else
                                        {{  $main_item->price }}
                                    @endif
                                </h6>
                            </div>
                        @endif
                        <div class="grouping-content-link">
                            @if (isset($main_item->price)
                            and !empty($main_item->price)
                            and isset($main_item->paypal)
                            and $main_item->price !== false
                            and $main_item->base_price !== false
                            and is_numeric($main_item->base_price)
                            and $main_item->paypal === true
                            and isset($settings->web_paypal)
                            and !empty($settings->web_paypal))
                            @if (!isset($settings->web_snipcart))
                                <form action="{{ Config::get('artlook.paypal.url') }}"
                                      method="post"
                                      id="paypalForm"
                                      class="form_paypal">
                                    <input type="hidden" name="cmd" value="{{ Config::get('artlook.paypal.param_cmd') }}">
                                    <input type="hidden" name="business" value="{{ $settings->web_paypal }}">
                                    <input type="hidden" name="lc" value="{{ App::getLocale() }}">
                                    <input type="hidden" name="item_name" value="{{ $main_item->title }}">
                                    <input type="hidden" name="item_number" value="{{ $main_item->id }}">
                                    <input type="hidden" name="quantity" value="{{ Config::get('artlook.paypal.param_quantity') }}">
                                    <input type="hidden" name="amount" value="{{ number_format($main_item->base_price, 2, '.', '') }}">
                                    <input type="hidden" name="currency_code" value="{{ $main_item->currency }}">
                                    <input type="hidden" name="bn" value="{{ Config::get('artlook.paypal.param_bn') }}">
                                    <input type="hidden" name="no_note" value="{{ Config::get('artlook.paypal.param_no_note') }}">
                                    <input type="hidden" name="no_shipping" value="{{ Config::get('artlook.paypal.param_no_shipping') }}">
                                    @if(isset($settings->paypal_notify_url) and !empty($settings->paypal_notify_url))
                                        <input type="hidden" name="notify_url" value="{!! $settings->paypal_notify_url !!}">
                                    @endif
                                    <input type="hidden" name="return" value="{{ Request::url() }}">
                                    <input type="hidden" name="cancel_return" value="{{ Request::url() }}">
                                    <input type="hidden" name="cn" value="{{ Config::get('artlook.paypal.param_cn') }}">
                                    <input type="hidden" name="custom" id="uniqueRef" value="">
                                    <input class="btn btn-alt" type="submit"
                                           name="Paypal" value="@lang('artlook::artlook.paypal')">
                                    <img alt="" border="0" src="{{ Config::get('artlook.paypal.img') }}"
                                         width="1" height="1">
                                </form>

                            @elseif(isset($settings->web_snipcart))

                                <button type="button" class="snipcart-add-item btn btn-success"
                                        data-item-id="{{ $main_item->id }}"
                                        data-item-price="{{ $main_item->base_price }}"
                                        data-item-url="{{ Request::fullUrl() }}"
                                        data-item-description="{{ $main_item->title }} - {{ $main_item->artist }}"
                                        data-item-image="{{ $main_item->image }}"
                                        data-item-name="{{ $main_item->title }}"
                                        data-item-weight="{{$main_item->weight}}"
                                        data-item-custom1-type="hidden"
                                        data-item-custom1-name="tenant"
                                        data-item-custom1-value="{{ $settings->tenant_id }}"
                                        data-item-max-quantity="{{ $main_item->availability }}"
                                        data-item-min-quantity="1">
                                    Add to basket
                                </button>
                            @endif
                        @endif
{{--                            @else--}}
                                <a href="{{ RouteHelper::route('user_space_contact', [
                                'user_space' => app('request')->user_space,
                                    'contact_type' => 'item_more_info',
                                'item_id' => $main_item->id,
                                'item_title' => $main_item->title
                                ]) }}" class="btn">@lang('artlook::artlook.enquire')</a>
{{--                            @endif--}}
                        </div>
                        @if(isset($main_item->description) && !empty($main_item->description))
                            <div class="grouping-content-text">
                                {!! $main_item->description !!}
                            </div>
                        @endif
                        <div class="grouping-content-social">
                            <ul>
                                <li><span>@lang('artlook::artlook.share'):</span></li>
                                <li class="share-content" data-share-network="facebook"><a
                                            href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(Request::fullUrl()) }}&display=popup"><i
                                                class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li class="share-content" data-share-network="twitter"><a
                                            href="https://twitter.com/intent/tweet?text={{  urlencode(html_entity_decode($main_item->title)) }}&url={{ urlencode(Request::fullUrl()) }}"><i
                                                class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li class="share-content" data-share-network="pinterest"><a
                                            href="javascript:void(0);"><i class="fa fa-pinterest"
                                                                          aria-hidden="true"></i></a>
                                    <div class="embeded-share-functionallity" style="display:none;">
                                        <a href='javascript:void((function()%7Bvar%20e=document.createElement(&apos;script&apos;);e.setAttribute(&apos;type&apos;,&apos;text/javascript&apos;);e.setAttribute(&apos;charset&apos;,&apos;UTF-8&apos;);e.setAttribute(&apos;src&apos;,&apos;http://assets.pinterest.com/js/pinmarklet.js?r=&apos;+Math.random()*99999999);document.body.appendChild(e)%7D)());'>
                                            <img src="{{ $main_item->image }}"/>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </article>
            </div>
        </section>
        <!-- END GROUPING //////////////////////////////////////////////////////   -->
    @endif
@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

    <script>
    $('#paypalForm').submit(function (e) {
        e.preventDefault();
        axios.post('{{ env('APP_API_URL') }}/lock',
        {
            id: '{{ $main_item->id }}',
            txn_id: 'Paypal-Lock'
        })
        .then(function (response) {
            console.log(response.data.result);
            if(response.data.result === 'success' && response.data.ref) {
                $('#uniqueRef').val(response.data.ref);
                e.currentTarget.submit();
                // alert('OK to submit')
            } else
                alert('That item is no longer available')
            })
        .catch(function (error) {
            console.log(error);
        });
    })


    @if(isset($settings->web_snipcart))

        function checkItem(parsedCartItem) {
            // console.log(parsedCartItem);
            axios.post('{{ env('APP_API_URL') }}/lock',
                {
                    id: '{{ $main_item->id }}',
                    txn_id: 'Paypal-Lock',
                    lock_session: parsedCartItem.uniqueId
                })
                .then(function (response) {
                    console.log(response.data.result);
                    if(response.data.result === 'success' && response.data.ref) {
                        console.log(response.data.ref);
                    } else {
                        alert('That item is no longer available');
                        Snipcart.api.cart.items.remove(parsedCartItem.uniqueId);
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
        }

    document.addEventListener('snipcart.ready', () => {

        Snipcart.events.on('item.added', (parsedCartItem) => {
            checkItem(parsedCartItem);
        });

        Snipcart.events.on('item.removed', (parsedCartItem) => {
            axios.post('{{ env('APP_API_URL') }}/unlock',
                {
                    id: '{{ $main_item->id }}',
                    locked_by: -99,
                    lock_session: parsedCartItem.uniqueId
                })
                .then(function (response) {
                    console.log(response.data.result);
                })
                .catch(function (error) {
                    console.log(error);
                });
        });


        /*Snipcart.events.on('item.updated', (parsedCartItem) => {
            console.log(parsedCartItem);
        });*/
    });



    @endif


    </script>
@endsection