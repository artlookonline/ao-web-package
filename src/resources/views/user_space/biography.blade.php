@extends('artlook::user_space.layouts.default')

@push('body_class', ' bio ')

@section('title', $settings->web_biography)

@section('head_og_type', '<meta property="og:type" content="profile"/>')
@if (isset($main_item) and !is_null($main_item))
    @section('head_og_image')
        <meta property="og:image" content="'.$main_item->image.'"/>
    @endsection
@endif

@section('main')
    @if (isset($main_item) and !empty($main_item))
        <!-- GROUPING LAYOUT//////////////////////////////////////////////////////   -->
        <section class="grouping grouping-layout {{ (!isset($main_item->image) or empty($main_item->image)) ? 'grouping-only-text' : '' }}">
            <div class="container">
                <article class="row">
                    <div class="grouping-layout-title">
                        <h1 title="{{ htmlspecialchars($settings->web_biography) }}">{{ $settings->web_biography }}</h1>
                    </div>
                    @if(isset($main_item->image) and !empty($main_item->image))
                        <div class="grouping-image">
                            <img src="{{ ViewHelper::getAPICacheImagePath($main_item->image, $settings->tenant_id, 'w768') }}" />
                        </div>
                    @endif
                    @if(isset($main_item->biography) and !empty($main_item->biography))
                        <div class="grouping-content">
                            <div class="grouping-content-text">
                                {!! $main_item->biography !!}
                            </div>
                        </div>
                    @endif
                </article>
            </div>
        </section>
        <!-- END GROUPING //////////////////////////////////////////////////////   -->
    @endif
@endsection