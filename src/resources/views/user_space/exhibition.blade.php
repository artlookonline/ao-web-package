@extends('artlook::user_space.layouts.default')

@inject('viewController', 'Artlook\Frontend\Http\Controllers\ViewController')

@push('body_class', ' exhibition ')

@php
    $masonryClasses = ['container' => '', 'item' => ''];
@endphp

@if (isset($settings->web_template)
and (
intval($settings->web_template) === 2
or intval($settings->web_template) === 3
))
    @push('body_class', ' masonry ')
    @php
        $masonryClasses = ['container' => 'masonry-container', 'item' => 'masonry-item'];
    @endphp
@endif

@section('title', $title)

@section('main')
    @if (isset($main_item) and !is_null($main_item))
        <!-- GROUPING LAYOUT//////////////////////////////////////////////////////   -->
        <section class="grouping grouping-layout grouping-layout-exhibition-detail">
            <div class="container">
                <article class="row">
                    <div class="grouping-layout-title">
                        <h1>{{ isset($main_item->full_name) ? $main_item->full_name : $main_item->title }}</h1>
                    </div>
                    @if ((isset($main_item->dateStart) and !empty($main_item->dateStart))
                           or (isset($main_item->dateEnd) and !empty($main_item->dateEnd))
                           or (isset($main_item->location) and !empty($main_item->location)))
                        <div class="grouping-content-data">
                            <ul>
                                @if ((isset($main_item->dateStart) and !empty($main_item->dateStart))
                                    or (isset($main_item->dateEnd) and !empty($main_item->dateEnd)))
                                    <li class="grouping-content-data-data">{{ (isset($main_item->dateStart) and !empty($main_item->dateStart)) ? $main_item->dateStart : '' }} {{ (isset($main_item->dateEnd) and !empty($main_item->dateEnd)) ? trans('artlook::artlook.to') . $main_item->dateEnd : '' }}</li>
                                @endif
                                @if (isset($main_item->location) and !empty($main_item->location))
                                    <li class="grouping-content-data-location">{{ strip_tags($main_item->location) }}</li>
                                @endif
                            </ul>
                        </div>
                    @endif
                    @if (isset($main_item->all_images) and !empty($main_item->all_images))
                        <div class="grouping-image">
                            {!! $viewController->getCarousel(app('request'), $main_item, 'w768', 'exhibitionMainCarousel') !!}
                        </div>
                    @endif
                    @if(isset($main_item->description) and !empty($main_item->description))
                        <div class="grouping-content">
                            <div class="grouping-content-text">
                                {!! $main_item->description !!}
                            </div>
                        </div>
                    @endif
                    @if (isset($main_item->items) and !empty($main_item->items))
                        <div class="grouping-gallery">
                            <ul class="row {{ $masonryClasses['container'] }}">
                                @if (isset($main_item->artist_indexed) and $main_item->artist_indexed === 1
                                and isset($main_item->artists) and !empty($main_item->artists))
                                    @foreach ($main_item->artists as $artist_id => $artist_name)
                                        <li class="{{ $masonryClasses['item'] }}">
                                            <a href="{{ RouteHelper::route('user_space_exhibition_artist', [ 'user_space' => app('request')->user_space, 'exhibition_id' => $main_item->id, 'exhibition_slug' => ViewHelper::getSlug($main_item->title), 'artist_slug' => ViewHelper::getSlug($artist_name), 'artist_id' => $artist_id ]) }}">
                                                <figure>
                                                    <span>
                                                        <img src="{{ ViewHelper::getArtistAPICacheImage($artist_id, $settings->tenant_id, 'w768') }}">
                                                    </span>
                                                    <figcaption>
                                                        {{ $artist_name }}
                                                    </figcaption>
                                                </figure>
                                            </a>
                                        </li>
                                    @endforeach
                                @else
                                    @foreach ($main_item->items as $currentItem)
                                        <li class="{{ $masonryClasses['item'] }}">
                                            <a href="{{ RouteHelper::route('user_space_exhibition_item', [
                                        'user_space' => app('request')->user_space,
                                        'exhibition_id' => $main_item->id,
                                        'item_id' => $currentItem->id,
                                        'item_slug' => ViewHelper::getSlug($currentItem->title)
                                        ]) }}">
                                                <figure><span><img
                                                                src="{{ ViewHelper::getAPICacheImagePath($currentItem->image, $settings->tenant_id, 'w768') }}"></span>
                                                    <figcaption>@if($settings->type === 'gallery')<small class="item-artist-name">{{ $currentItem->artist }}</small>@endif{{ $currentItem->title }}</figcaption>
                                                </figure>
                                            </a>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    @endif
                    @if(isset($main_item->map) and !empty($main_item->map))
                        <div class="grouping-map">
                            {!! $main_item->map !!}
                        </div>
                    @endif
                </article>
            </div>
        </section>
        <!-- END GROUPING //////////////////////////////////////////////////////   -->
    @endif
@endsection
