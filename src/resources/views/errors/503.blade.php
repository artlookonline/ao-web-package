@extends('layouts.errors')

@section('title', 'Service Unavailable')

@section('content')
    <div class="title">
        {{ $exception->getStatusCode() }}
    </div>
@endsection