var App;

(function ($) {
    // Use this variable to set up the common and page specific functions. If you
    // rename this variable, you will also need to rename the namespace below.
    App = {
        // Configuration
        'debug': false,
        // functions
        widthDeviceHandler: function () {
            var self = this;
            var $widthDeviceControl = $('#widthDeviceControl');
            $widthDeviceControl.show();
            $widthDeviceControl.children('.div-device-control').each(function () {
                if ($(this).is(":visible")) {
                    if ($(this).hasClass('visible-lg')) {
                        self.display = 'visible-lg';
                    } else if ($(this).hasClass('visible-md')) {
                        self.display = 'visible-md';
                    } else if ($(this).hasClass('visible-sm')) {
                        self.display = 'visible-sm';
                    } else if ($(this).hasClass('visible-xs')) {
                        self.display = 'visible-xs';
                    } else if ($(this).hasClass('visible-xxs')) {
                        self.display = 'visible-xxs';
                    } else {
                        delete self.display;
                    }
                }
            });
            $widthDeviceControl.hide();
            if (self.display === 'visible-xxs' || self.display === 'visible-xs') {
                App.getInformedStartScrollListener = false;
            } else {
                App.getInformedStartScrollListener = true;
            }
            if (App.debug) {
                console.log('self.display', self.display);
            }
        },
        headerHandler: function () {
            var self = this;
            var topBreakPoint = 240;
            var middleBreakPoint = 30;
            switch (self.display) {
                case 'visible-xs':
                case 'visible-xxs':
                    topBreakPoint = 268;
                    middleBreakPoint = 30;
                    break;
            }
            if (App.debug) {
                console.log('topBreakPoint', topBreakPoint);
            }
            if ($(window).scrollTop() >= (topBreakPoint - middleBreakPoint) && ($(document).height() - $(window).height() > (topBreakPoint - middleBreakPoint))) {
                $(document.body).addClass("main-header-fixed-in");
            } else {
                $(document.body).removeClass("main-header-fixed-in");
            }
            if ($(window).scrollTop() >= topBreakPoint && ($(document).height() - $(window).height() > topBreakPoint)) {
                $(document.body).removeClass("main-header-fixed-in");
                $(document.body).addClass("main-header-fixed");
            } else {
                $(document.body).removeClass("main-header-fixed");
            }
        },
        clearForm: function ($item) {
            return $item.each(function () {
                var type = ($(this).get(0).hasAttribute("type")) ? $(this).attr('type').toLowerCase() : undefined,
                    tag = $(this).get(0).tagName.toLowerCase(),
                    readonly = ($(this).get(0).hasAttribute("readonly")) ? true : false;
                if (tag === 'form') {
                    return App.clearForm($(':input', $(this).get(0)));
                }
                if (!readonly) {
                    if (type == 'text' || type == 'password' || type == 'email' || tag == 'textarea') {
                        $(this).val('');
                    } else if (type == 'checkbox' || type == 'radio') {
                        $(this).prop('checked', false);
                    } else if (tag == 'select') {
                        $(this).prop('selectedIndex', -1);
                    }
                }
            });
        },
        // All pages
        'common': {
            init: function () {
                if (App.debug) {
                    console.log('App ready!');
                }
                this.onLoadListener();
                this.onResizeListener();
                this.onFocusListener();
                this.scrollListener();
                this.scrollToTopListener();
                this.formValidateListener();
                this.formAjaxListener();
                this.shareContentListener();
            },
            onLoadListener: function () {
                $(window).on('load', function () {
                    App.widthDeviceHandler();
                    App.headerHandler();
                });
            },
            onResizeListener: function () {
                $(window).on('resize', function () {
                    App.widthDeviceHandler();
                    App.headerHandler();
                });
            },
            onFocusListener: function () {
                $(window).on('focus', function () {
                    App.widthDeviceHandler();
                    App.headerHandler();
                });
            },
            scrollListener: function () {
                var scrollToTopSelector = "#scrollToTop";
                $(window).on('scroll', function () {
                    if ($(window).scrollTop() > 0) {
                        $(scrollToTopSelector).stop().fadeIn("slow");
                    } else {
                        $(scrollToTopSelector).stop().fadeOut("slow");
                    }
                    App.headerHandler();
                    if (App.debug) {
                        console.log('$(window).scrollTop()', $(window).scrollTop());
                    }
                });
            },
            scrollToTopListener: function () {
                var selector = "#scrollToTop";
                $(selector).on("click", function (event) {
                    event.preventDefault();
                    $('html, body').animate(
                        {
                            scrollTop: 0
                        },
                        App.scrollTo.velocity,
                        App.scrollTo.easing
                    );
                });
            },
            formValidateListener: function () {
                $('form.form-validate').each(function () {
                    if (App.debug) {
                        console.log('form.form-validate listener!');
                    }
                    $(this).validate({
                        ignore: [],
                        rules: {
                            reCaptchaValidation: {
                                required: function () {
                                    if (grecaptcha.getResponse() == '') {
                                        return true;
                                    } else {
                                        return false;
                                    }
                                }
                            }
                        },
                        errorPlacement: function (error, element) {
                            error.insertAfter(element);
                        }
                    });
                });
            },
            formAjaxListener: function () {
                $('body').on('submit', 'form.form-ajax', function (event) {
                    event.preventDefault();
                    if (App.debug) {
                        console.log('form.form-ajax submit!');
                    }

                    var $form = $(this),
                        $submitButton = $form.find('button[type="submit"]');

                    $submitButton.button('loading');
                    $.ajax({
                        type: $(this).attr('method'),
                        url: $(this).attr('action'),
                        data: $(this).serialize()
                    })
                        .done(function (data) {
                            if (App.debug) {
                                console.log('done');
                                console.log('data', data);
                            }
                            App.clearForm($form);
                            swal({
                                title: (typeof data.title !== 'undefined') ? data.title : 'Success',
                                text: (typeof data.message !== 'undefined') ? data.message : 'Send.',
                                type: 'success',
                                confirmButtonText: 'Close'
                            });
                        })
                        .fail(function (jqXHR, textStatus, errorThrown) {
                            if (App.debug) {
                                console.log('fail');
                                console.log('jqXHR', jqXHR);
                                console.log('textStatus', textStatus);
                                console.log('errorThrown', errorThrown);
                            }
                            var responseText = jQuery.parseJSON(jqXHR.responseText);
                            if (App.debug) {
                                console.log('responseText', responseText);
                                console.log('responseText.title', responseText.title);
                            }
                            swal({
                                title: (typeof responseText.title !== 'undefined') ? responseText.title : 'Error',
                                text: (typeof responseText.message !== 'undefined') ? responseText.message : 'Ops!',
                                type: 'error',
                                confirmButtonText: 'Close'
                            });
                        })
                        .always(function () {
                            if (App.debug) {
                                console.log('always');
                            }
                            $submitButton.button('reset');
                            grecaptcha.reset(window.grecaptchaWidget);
                        });
                });
            },
            shareContentListener: function () {
                var self = this;
                var shareContentSelector = ".share-content > a";
                $(shareContentSelector).on("click", function (event) {
                    event.preventDefault();
                    var $parent = $(this).closest('li.share-content');
                    var network = $parent.data('shareNetwork');
                    if (App.debug) {
                        console.log("$(this)", $(this));
                        console.log("$parent", $parent);
                        console.log("network", network);
                    }
                    switch (network) {
                        case 'twitter':
                            var url = $(this).attr('href');
                            self.shareContentPopup(url, 'twitterSharePopup', 520, 350);
                            break;
                        case 'facebook':
                            var url = $(this).attr('href');
                            self.shareContentPopup(url, 'facebookSharePopup', 520, 350);
                            break;
                        case 'google-plus':
                            var url = $(this).attr('href');
                            self.shareContentPopup(url, 'googlePlusSharePopup', 520, 350);
                            break;
                        case 'pinterest':
                            var embededShareFunctionallity = $parent.find('.embeded-share-functionallity');
                            var embededShareFunctionallityImage = embededShareFunctionallity.find('a img');
                            if (App.debug) {
                                console.log("embededShareFunctionallity", embededShareFunctionallity);
                                console.log("embededShareFunctionallityImage", embededShareFunctionallityImage);
                            }
                            embededShareFunctionallityImage.trigger('click');
                            break;
                        default:
                            break;
                    }
                    return;
                });
            },
            shareContentPopup: function (url, name, winWidth, winHeight) {
                var dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : screen.left;
                var dualScreenTop = window.screenTop !== undefined ? window.screenTop : screen.top;

                var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
                var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

                var winLeft = ((width / 2) - (winWidth / 2)) + dualScreenLeft;
                var winTop = ((height / 2) - (winHeight / 2)) + dualScreenTop;
                if (App.debug) {
                    console.log('winTop', winTop);
                    console.log('winLeft', winLeft);
                }
                window.open(url, name, 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=1,scrollbars=1,width=' + winWidth + ',height=' + winHeight);
            }
        },
        'item': {
            init: function () {
                this.imageLightboxHandler();
            },
            imageLightboxHandler: function () {
                if (App.debug) {
                    console.log('imageLightboxHandler');
                }
                $('.lightbox').swipebox();
            }
        },
        'contact': {
            init: function () {
                if ($('form.grouping-content-contact').length > 0) {
                    var honeypot = $('form.grouping-content-contact').honey();
                    honeypot.name('company');
                }
            }
        },
        'masonry': {
            init: function () {
                this.masonryBootstrap();
            },
            masonryBootstrap: function (containerSelector, itemSelector) {
                containerSelector = (containerSelector !== undefined) ? containerSelector : '.masonry-container';
                itemSelector = (itemSelector !== undefined) ? itemSelector : '.masonry-item';
                if ($(containerSelector).length >= 1
                    && $(itemSelector).length >= 1) {
                    var $masonryContainer = $(containerSelector);

                    var $masonryGrid = $masonryContainer.imagesLoaded(function () {
                        $masonryGrid.masonry({
                            itemSelector: itemSelector,
                            columnWidth: $masonryContainer.find(itemSelector)[0],
                            percentPosition: true
                        });
                    });

                    $(window).on('resize', function () {
                        App.masonry.masonryResize(containerSelector);
                    });
                }
            },
            masonryResize: function (containerSelector) {
                $(containerSelector).masonry('reloadItems');
            }
        }
    };
    // The routing fires all common scripts, followed by the page specific scripts.
    // Add additional events for more control over timing e.g. a finalize event
    var UTIL = {
        fire: function (func, funcname, args) {
            var fire;
            var namespace = App;
            funcname = (funcname === undefined) ? 'init' : funcname;
            fire = func !== '';
            fire = fire && namespace[func];
            fire = fire && typeof namespace[func][funcname] === 'function';

            if (fire) {
                namespace[func][funcname](args);
            }
        },
        loadEvents: function () {
            // Fire common init JS
            UTIL.fire('common');

            // Fire page-specific init JS, and then finalize JS
            $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function (i, classnm) {
                UTIL.fire(classnm);
                UTIL.fire(classnm, 'finalize');
            });

            // Fire common finalize JS
            UTIL.fire('common', 'finalize');
        }
    };
    // Load Events
    $(document).ready(UTIL.loadEvents);
})(jQuery);