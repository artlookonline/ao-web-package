<?php

namespace Artlook\Frontend\Providers;

use Illuminate\Routing\Route;
use Illuminate\Support\ServiceProvider;
use Artlook\Frontend\Helpers\RouteHelper;

class RouteHelperServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('helper.route', function () {
            return new RouteHelper();
        });
    }
}
