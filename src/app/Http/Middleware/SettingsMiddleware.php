<?php

namespace Artlook\Frontend\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class SettingsMiddleware
{
    /**
     * Handle an incoming request. DETERMINE WHICH DOMAIN.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $mainDomain = config('artlook.settings.main_domain');

        $domainType = 'subdomain';
        $useUserTypeForTheRoute = true;

        $apiBridgeHelper = app()->make('helper.api_bridge');

        $settings = null;

        if ($request->getHost() === $mainDomain) {
            $settings = $apiBridgeHelper->getData($request, 'artlook.api.endpoints.user.settings', [
                'user_space' => $request->user_space
            ]);
        } else {
            $settings = $apiBridgeHelper->getData($request, 'artlook.api.endpoints.user.checkurl', [
                'host' => $request->getHost()
            ]);
            $request->route()->setParameter('user_space', $settings->slug);
            $mainDomain = $request->getHost();
            $domainType = 'domain';
            $useUserTypeForTheRoute = false;

        }

        if (is_null($settings)) {
            abort(404);
        }

        if (env('APP_ENV') === 'local' && !empty(env('APP_USER_SPACE_FORCED_WEB_TEMPLATE'))) {
            $settings->web_template = env('APP_USER_SPACE_FORCED_WEB_TEMPLATE');
        }

        if (env('APP_ENV') === 'local' && !empty(env('APP_USER_SPACE_FORCED_WEB_COLOURS'))) {
            $settings->web_colours = env('APP_USER_SPACE_FORCED_WEB_COLOURS');
        }

        if (env('APP_ENV') === 'local' && !empty(env('APP_USER_SPACE_FORCED_WEB_FONTS'))) {
            $settings->web_fonts = env('APP_USER_SPACE_FORCED_WEB_FONTS');
        }

        $request->attributes->add([
            'settings' => $settings,
            'main_domain' => $mainDomain,
            'domain_type' => $domainType,
            'use_user_type_for_the_route' => $useUserTypeForTheRoute
        ]);
		
		\App::setLocale($settings->web_language);

        view()->composer('*', function ($view) use ($settings, $mainDomain, $domainType, $useUserTypeForTheRoute) {
            $view->with('settings', $settings)
            ->with('main_domain', $mainDomain)
            ->with('domain_type', $domainType)
            ->with('use_user_type_for_the_route', $useUserTypeForTheRoute);
        });

        return $next($request);
    }
}
