<?php

namespace Artlook\Frontend\Http\Controllers;

use Artlook\Frontend\Helpers\RouteHelper;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
//use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail as Mail;
use GuzzleHttp\Client as Client;
use Artlook\Frontend\Http\Controllers\AbstractController as BaseController;
use Artlook\Frontend\Helpers\ViewHelper;
use Illuminate\Support\Facades\Route;

use Illuminate\Support\Facades\Log;

class UserSpaceController extends BaseController {
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	/**
	 * Return the
	 * index page.
	 *
	 * @return string
	 */

	public function index( Request $request, $userSpace ) {
		$userSpace = $request->route()->hasParameter( 'user_space' ) ? $request->route()->parameter( 'user_space' ) : $userSpace;

		$data = $this->getData( $request, __FUNCTION__, [
			'user_space' => $userSpace
		] );

		$view = view( 'artlook::user_space.home' );

		if ( ! is_null( $data ) ) {
			/**
			 * Main item
			 */
			$mainItem = array_shift( $data );

			$allImages            = $this->getAllImages( $mainItem );
			$mainItem->all_images = $allImages;

			if ( ! empty( $allImages )
			     && ( ! isset( $mainItem->description ) || empty( $mainItem->description ) )
			) {
				$mainItem->grouping_classes = [ 'grouping-only-image' ];
				$mainItem->only_image_item  = true;
			} elseif ( empty( $allImages )
			           && ( isset( $mainItem->description ) && ! empty( $mainItem->description ) )
			) {
				$mainItem->grouping_classes = [ 'grouping-only-text' ];
				$mainItem->only_text_item   = true;
			}

			$view->with( 'main_item', $mainItem );

			/**
			 * Rest items
			 */
			if ( ! empty( $data ) ) {
				foreach ( $data as $key => $item ) {
					$allImages                = $this->getAllImages( $item );
					$data[ $key ]->all_images = $allImages;

					if ( ! empty( $allImages )
					     && ( ! isset( $item->description ) || empty( $item->description ) )
					) {
						$item->grouping_classes = [ 'grouping-only-image' ];
						$item->only_image_item  = true;
					} elseif ( empty( $allImages )
					           && ( isset( $item->description ) && ! empty( $item->description ) )
					) {
						$item->grouping_classes = [ 'grouping-only-text' ];
						$item->only_text_item   = true;
					}
				}
				$view->with( 'subsidiaries_items', $data );
			}
		}

		return $view
			->with( 'title', 'Home' );
	}

	/**
	 * Return the user
	 * biography
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param $userSpace
	 *
	 * @return string
	 */

	public function biography( Request $request, $userSpace ) {
		$userSpace = $request->route()->hasParameter( 'user_space' ) ? $request->route()->parameter( 'user_space' ) : $userSpace;

		$data = $this->getData( $request, __FUNCTION__, [
			'user_space' => $userSpace
		] );

		$view = view( 'artlook::user_space.biography' );

		if ( ! is_null( $data ) ) {
			$view->with( 'main_item', $data );
		}

		return $view
			->with( 'title', 'Biography' );
	}

	/**
	 * Return the user
	 * contact
	 *
	 * @param $request
	 *
	 * @return string
	 */

	public function contact( Request $request ) {
		$settings = $request->attributes->get( 'settings' );

		$view = view( 'artlook::user_space.contact' );

		return $view
			->with( 'title', ( ! isset( $settings->type ) || $settings->type === 'artist' ? 'Contact' : 'How to find us or contact us' ) );
	}

	/**
	 * Return the user
	 * contact
	 *
	 * @param $request
	 *
	 * @return string
	 */

	public function contactForm( Request $request ) {

        if ( ! $request->has( 'g-recaptcha-response' ) ) {
			return response()
				->json( [
					'success' => false,
					'title'   => 'Error',
					'message' => 'Please confirm you\'re not a robot with reCAPTCHA'
				], 500 );
		}

		/**
		 * @var $client Client;
		 */
		$client = new Client();
		try {
			$response = $client->request( 'POST', config( 'artlook.recaptcha.api_request' ), [
				'verify' => false,
				'query'  => [
					'secret'   => config( 'artlook.recaptcha.api_secret' ),
					'response' => $request->get( 'g-recaptcha-response' ),
					'remoteip' => $request->ip()
				]
			] );
		} catch ( \Exception $e ) {
			return response()
				->json( [
					'success' => false,
					'title'   => 'Error',
					'message' => 'There has been a problem with sending your enquiry, please try again.'
				], 500 );
		}

		if($request->has('website') && $request->website > '') {
		    //honeypot field "website" has been filled in so probably a bot.
            //Log the details
            Log::info('spam detected', ['request' => $request]);
            //pretend to the spammer that the mailhas been sent
            return response()
                ->json( [
                    'success' => true,
                    'title'   => 'Thank you.',
                    'message' => 'Your message has been processed.' //note "processed" not "sent" - so we can test it
                ], 200 );
        }

        if($request->has('note') && $request->note > '') {
            //check if text contains a URL
            $hasURL = strpos($request->note, 'http:') !== false || strpos($request->note, 'https:') !== false ;
            if ($hasURL) {

                //Log the details
                Log::info('spam detected - contains URL', ['request' => $request]);
                //pretend to the spammer that the mailhas been sent
                return response()
                    ->json([
                        'success' => true,
                        'title' => 'Thank you.',
                        'message' => 'Your message has been processed.' //note "processed" not "sent" - so we can test it
                    ], 200);
            }
        }

		$body = $response->getBody();
		$data = json_decode( $body );

		if ( ! isset( $data->success ) || $data->success === false || $data->hostname !== $request->attributes->get( 'main_domain' ) ) {
			return response()
				->json( [
					'success' => false,
					'title'   => 'Error',
					'message' => 'There has been a problem with sending your enquiry, please try again.'
				], 500 );
		}

		$settings = $request->attributes->get( 'settings' );

		$subject   = ( config( 'artlook.email.contact_form.subject' ) );
		$templates = [
			'artlook::user_space.emails.artist.contact_notification_html',
			'artlook::user_space.emails.artist.contact_notification_text'
		];

		if ( $request->has( 'contact_type' ) ) {
			if ( $request->input( 'contact_type' ) === 'item_more_info'
			     && ( $request->has( 'subject' ) )
			) {
				$subject   = $request->input( 'subject' );
				$templates = [
					'artlook::user_space.emails.artist.enquire_item_notification_html',
					'artlook::user_space.emails.artist.enquire_item_notification_text'
				];
			}
		}

		try {
			Mail::send( $templates, array_merge( $request->all(), [ 'subject' => $subject ] ), function ( $m ) use ( $settings, $subject, $request ) {
				$m->from( config( 'artlook.email.contact_form.sender_email' ), config( 'artlook.email.contact_form.sender_name' ) )
                    ->replyTo( $request->email, $request->name )
                    ->to( $settings->orgemail )
				    ->subject( $subject );
			} );
		} catch ( \Exception $e ) {
			return response()
				->json( [
					'success' => false,
					'title'   => 'Error',
					'message' => 'There has been a problem with sending your enquiry, please try again.' . $e->getMessage()
				], 500 );
		}

		return response()
			->json( [
				'success' => true,
				'title'   => 'Thank you.',
				'message' => 'Your message has been sent.'
			], 200 );
	}

	/**
	 * Return the user
	 * exhibitions
	 *
	 * @param $request
	 * @param $userSpace
	 *
	 * @return string
	 */

	public function exhibitions( Request $request, $userSpace ) {
		$userSpace = $request->route()->hasParameter( 'user_space' ) ? $request->route()->parameter( 'user_space' ) : $userSpace;

		$data = $this->getData( $request, __FUNCTION__, [
			'user_space' => $userSpace
		] );

		$view = view( 'artlook::user_space.exhibitions' );

		if ( ! is_null( $data ) ) {
			/**
			 * Main item
			 */
			$mainItem = array_shift( $data );

			$allImages            = $this->getAllImages( $mainItem );
			$mainItem->all_images = $allImages;

			if ( empty( $allImages )
			     && ( isset( $mainItem->description ) && ! empty( $mainItem->description ) )
			) {
				$mainItem->grouping_classes = [ 'grouping-only-text' ];
				$mainItem->only_text_item   = true;
			}

			$view->with( 'main_item', $mainItem );

			/**
			 * Rest items
			 */
			if ( ! empty( $data ) ) {
				foreach ( $data as $key => $item ) {
					$allImages                = $this->getAllImages( $item );
					$data[ $key ]->all_images = $allImages;

					if ( empty( $allImages ) ) {
						$data[ $key ]->grouping_classes = [ 'grouping-only-text' ];
						$data[ $key ]->only_text_item   = true;
					}
				}
				$view->with( 'subsidiaries_items', $data );
			}
		}

		return $view
			->with( 'title', 'Exhibitions' );
	}

	/**
	 * Return a user
	 * exhibition
	 *
	 * @param $request
	 * @param $userSpace
	 * @param $id
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */

	public function exhibition( Request $request, $userSpace, $id ) {
		$userSpace = $request->route()->hasParameter( 'user_space' ) ? $request->route()->parameter( 'user_space' ) : $userSpace;
		$id        = $request->route()->hasParameter( 'id' ) ? $request->route()->parameter( 'id' ) : $id;

		$data = $this->getData( $request, __FUNCTION__, [
			'user_space' => $userSpace,
			'id'         => $id
		] );

		$view = view( 'artlook::user_space.exhibition' );

		if ( ! is_null( $data ) ) {
			if ( is_array( $data ) ) {
				$data = current( $data );
			}
			$allImages        = $this->getAllImages( $data );
			$data->all_images = $allImages;

			$view->with( 'main_item', $data )
			     ->with( 'title', $data->title );
		}

		return $view;
	}

	/**
	 * Return a user
	 * exhibition item
	 *
	 * @param Request $request
	 * @param $userSpace
	 * @param $exhibitionId
	 * @param $itemId
	 * @param $itemSlug
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */

	public function exhibitionItem( Request $request, $userSpace, $exhibitionId, $itemId, $itemSlug, $parameters = null ) {
		$userSpace    = $request->route()->hasParameter( 'user_space' ) ? $request->route()->parameter( 'user_space' ) : $userSpace;
		$exhibitionId = $request->route()->hasParameter( 'exhibition_id' ) ? $request->route()->parameter( 'exhibition_id' ) : $exhibitionId;
		$itemId       = $request->route()->hasParameter( 'item_id' ) ? $request->route()->parameter( 'item_id' ) : $itemId;
		$itemSlug     = $request->route()->hasParameter( 'item_slug' ) ? $request->route()->parameter( 'item_slug' ) : $itemSlug;
		$parameters   = $request->route()->hasParameter( 'parameters' ) ? $request->route()->parameter( 'parameters' ) : $parameters;

		$data = $this->getData( $request, 'exhibition', [
			'user_space' => $userSpace,
			'id'         => $exhibitionId
		] );

		$parent = null;
		if ( ! is_null( $data ) ) {
			if ( is_array( $data ) ) {
				$parent = current( $data );
			} else {
				$parent = $data;
			}
		}

		return $this->item( $request, $userSpace, $itemId, $itemSlug, $parent, $parameters );
	}

    public function exhibitionArtist( Request $request, $userSpace, $exhibitionId, $exhibitionSlug, $artistSlug, $artistId, $parameters = null ) {
        $userSpace  = $request->route()->hasParameter( 'user_space' ) ? $request->route()->parameter( 'user_space' ) : $userSpace;
        $exhibitionId     = $request->route()->hasParameter( 'exhibition_id' ) ? $request->route()->parameter( 'exhibition_id' ) : $exhibitionId;
        $exhibitionSlug   = $request->route()->hasParameter( 'exhibition_slug' ) ? $request->route()->parameter( 'exhibition_slug' ) : $exhibitionSlug;
        $artistId     = $request->route()->hasParameter( 'artist_id' ) ? $request->route()->parameter( 'artist_id' ) : $artistId;
        $artistSlug   = $request->route()->hasParameter( 'artist_slug' ) ? $request->route()->parameter( 'artist_slug' ) : $artistSlug;
        $parameters = $request->route()->hasParameter( 'parameters' ) ? $request->route()->parameter( 'parameters' ) : $parameters;

        $data = $this->getData( $request, 'exhibition', [
            'user_space' => $userSpace,
            'id'         => $exhibitionId
        ] );

        $parent = null;
        if ( ! is_null( $data ) ) {
            if ( is_array( $data ) ) {
                $parent = current( $data );
            } else {
                $parent = $data;
            }
        }

        return $this->artist( $request, $userSpace, $artistId, $artistSlug, $parent, $parameters );
    }

    public function exhibitionArtistItem( Request $request, $userSpace, $exhibitionId, $exhibitionSlug, $artistSlug, $artistId, $itemSlug, $itemId, $parameters = null ) {
        $userSpace  = $request->route()->hasParameter( 'user_space' ) ? $request->route()->parameter( 'user_space' ) : $userSpace;
        $exhibitionId     = $request->route()->hasParameter( 'exhibition_id' ) ? $request->route()->parameter( 'exhibition_id' ) : $exhibitionId;
        $exhibitionSlug   = $request->route()->hasParameter( 'exhibition_slug' ) ? $request->route()->parameter( 'exhibition_slug' ) : $exhibitionSlug;
        $artistId     = $request->route()->hasParameter( 'artist_id' ) ? $request->route()->parameter( 'artist_id' ) : $artistId;
        $artistSlug   = $request->route()->hasParameter( 'artist_slug' ) ? $request->route()->parameter( 'artist_slug' ) : $artistSlug;
        $itemId     = $request->route()->hasParameter( 'item_id' ) ? $request->route()->parameter( 'item_id' ) : $itemId;
        $itemSlug   = $request->route()->hasParameter( 'item_slug' ) ? $request->route()->parameter( 'item_slug' ) : $itemSlug;
        $parameters = $request->route()->hasParameter( 'parameters' ) ? $request->route()->parameter( 'parameters' ) : $parameters;

        $data = $this->getData( $request, 'exhibition', [
            'user_space' => $userSpace,
            'id'         => $exhibitionId
        ] );

        $parent = null;
        if ( ! is_null( $data ) ) {
            if ( is_array( $data ) ) {
                $parent = current( $data );
            } else {
                $parent = $data;
            }
        }

        return $this->item( $request, $userSpace, $itemId, $itemSlug, $parent, $parameters );
    }

	/**
	 * Return the user
	 * galleries
	 *
	 * @param $request
	 * @param $userSpace
	 *
	 * @return string
	 */

	public function galleries( Request $request, $userSpace ) {
		$userSpace = $request->route()->hasParameter( 'user_space' ) ? $request->route()->parameter( 'user_space' ) : $userSpace;

		$data = $this->getData( $request, __FUNCTION__, [
			'user_space' => $userSpace
		] );

		$view = view( 'artlook::user_space.galleries' );

		if ( ! is_null( $data ) ) {
			$view->with( 'main_item', array_shift( $data ) )
			     ->with( 'subsidiaries_items', $data );
		}

		return $view
			->with( 'title', 'Galleries' );
	}

	/**
	 * Return a user
	 * gallery
	 *
	 * @param $request
	 * @param $userSpace
	 * @param $id
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */

	public function gallery( Request $request, $userSpace, $id ) {
		$userSpace = $request->route()->hasParameter( 'user_space' ) ? $request->route()->parameter( 'user_space' ) : $userSpace;
		$id        = $request->route()->hasParameter( 'id' ) ? $request->route()->parameter( 'id' ) : $id;

		$data = $this->getData( $request, __FUNCTION__, [
			'user_space' => $userSpace,
			'id'         => $id
		] );

		$view = view( 'artlook::user_space.gallery' );

		if ( ! is_null( $data ) ) {
			if ( is_array( $data ) ) {
				$data = current( $data );
			}
			$allImages        = $this->getAllImages( $data );
			$data->all_images = $allImages;

			$view->with( 'main_item', $data )
			     ->with( 'title', $data->title );
		}

		return $view;
	}

	/**
	 * Return a user
	 * gallery item
	 *
	 * @param Request $request
	 * @param $userSpace
	 * @param $galleryId
	 * @param $itemId
	 * @param $itemSlug
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */

	public function galleryItem( Request $request, $userSpace, $galleryId, $itemId, $itemSlug, $parameters = null ) {
		$userSpace  = $request->route()->hasParameter( 'user_space' ) ? $request->route()->parameter( 'user_space' ) : $userSpace;
		$galleryId  = $request->route()->hasParameter( 'gallery_id' ) ? $request->route()->parameter( 'gallery_id' ) : $galleryId;
		$itemId     = $request->route()->hasParameter( 'item_id' ) ? $request->route()->parameter( 'item_id' ) : $itemId;
		$itemSlug   = $request->route()->hasParameter( 'item_slug' ) ? $request->route()->parameter( 'item_slug' ) : $itemSlug;
		$parameters = $request->route()->hasParameter( 'parameters' ) ? $request->route()->parameter( 'parameters' ) : $parameters;

		$data = $this->getData( $request, 'gallery', [
			'user_space' => $userSpace,
			'id'         => $galleryId
		] );

		$parent = null;
		if ( ! is_null( $data ) ) {
			if ( is_array( $data ) ) {
				$parent = current( $data );
			} else {
				$parent = $data;
			}
		}

		return $this->item( $request, $userSpace, $itemId, $itemSlug, $parent, $parameters );
	}

    public function galleryArtist( Request $request, $userSpace, $galleryId, $gallerySlug, $artistSlug, $artistId, $parameters = null ) {
        $userSpace  = $request->route()->hasParameter( 'user_space' ) ? $request->route()->parameter( 'user_space' ) : $userSpace;
        $galleryId     = $request->route()->hasParameter( 'gallery_id' ) ? $request->route()->parameter( 'gallery_id' ) : $galleryId;
        $gallerySlug   = $request->route()->hasParameter( 'gallery_slug' ) ? $request->route()->parameter( 'gallery_slug' ) : $gallerySlug;
        $artistId     = $request->route()->hasParameter( 'artist_id' ) ? $request->route()->parameter( 'artist_id' ) : $artistId;
        $artistSlug   = $request->route()->hasParameter( 'gallery_slug' ) ? $request->route()->parameter( 'gallery_slug' ) : $artistSlug;
        $parameters = $request->route()->hasParameter( 'parameters' ) ? $request->route()->parameter( 'parameters' ) : $parameters;

        $data = $this->getData( $request, 'gallery', [
            'user_space' => $userSpace,
            'id'         => $galleryId
        ] );

        $parent = null;
        if ( ! is_null( $data ) ) {
            if ( is_array( $data ) ) {
                $parent = current( $data );
            } else {
                $parent = $data;
            }
        }

        return $this->artist( $request, $userSpace, $artistId, $artistSlug, $parent, $parameters );
    }

    public function galleryArtistItem( Request $request, $userSpace, $galleryId, $gallerySlug, $artistSlug, $artistId, $itemSlug, $itemId, $parameters = null ) {
        $userSpace  = $request->route()->hasParameter( 'user_space' ) ? $request->route()->parameter( 'user_space' ) : $userSpace;
        $galleryId     = $request->route()->hasParameter( 'gallery_id' ) ? $request->route()->parameter( 'gallery_id' ) : $galleryId;
        $gallerySlug   = $request->route()->hasParameter( 'gallery_slug' ) ? $request->route()->parameter( 'gallery_slug' ) : $gallerySlug;
        $artistId     = $request->route()->hasParameter( 'artist_id' ) ? $request->route()->parameter( 'artist_id' ) : $artistId;
        $artistSlug   = $request->route()->hasParameter( 'artist_slug' ) ? $request->route()->parameter( 'artist_slug' ) : $artistSlug;
        $itemId     = $request->route()->hasParameter( 'item_id' ) ? $request->route()->parameter( 'item_id' ) : $itemId;
        $itemSlug   = $request->route()->hasParameter( 'item_slug' ) ? $request->route()->parameter( 'item_slug' ) : $itemSlug;
        $parameters = $request->route()->hasParameter( 'parameters' ) ? $request->route()->parameter( 'parameters' ) : $parameters;

        $data = $this->getData( $request, 'gallery', [
            'user_space' => $userSpace,
            'id'         => $galleryId
        ] );

        $parent = null;
        if ( ! is_null( $data ) ) {
            if ( is_array( $data ) ) {
                $parent = current( $data );
            } else {
                $parent = $data;
            }
        }

        return $this->item( $request, $userSpace, $itemId, $itemSlug, $parent, $parameters );
    }

	/**
	 * Return the user
	 * artists
	 *
	 * @param $request
	 * @param $userSpace
	 *
	 * @return string
	 */

	public function artists( Request $request, $userSpace ) {
		$settings = $request->attributes->get( 'settings' );
		if ( $settings->type !== 'gallery' ) {
			abort( 404 );
		}

		$userSpace = $request->route()->hasParameter( 'user_space' ) ? $request->route()->parameter( 'user_space' ) : $userSpace;

		$data = $this->getData( $request, __FUNCTION__, [
			'user_space' => $userSpace, 'no-imagecheck' => 'true'
		] );

		$view = view( 'artlook::user_space.artists' );

		if ( ! is_null( $data ) ) {
			$view->with( 'main_item', array_shift( $data ) )
			     ->with( 'subsidiaries_items', $data );
		}

		return $view
			->with( 'title', 'Artists' );
	}

	/**
	 * Return a user
	 * artist
	 *
	 * @param $request
	 * @param $userSpace
	 * @param $id
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */

	public function artist( Request $request, $userSpace, $id, $slug, $parent = null, $parameters = null ) {
		$settings = $request->attributes->get( 'settings' );
		if ( $settings->type !== 'gallery' ) {
			abort( 404 );
		}

		$userSpace = $request->route()->hasParameter( 'user_space' ) ? $request->route()->parameter( 'user_space' ) : $userSpace;
		$id        = $request->route()->hasParameter( 'id' ) ? $request->route()->parameter( 'id' ) : $id;
        $parent     = $request->route()->hasParameter( 'parent' ) ? $request->route()->parameter( 'parent' ) : $parent;
        $parameters = $request->route()->hasParameter( 'parameters' ) ? $request->route()->parameter( 'parameters' ) : $parameters;

		$data = $this->getData( $request, __FUNCTION__, [
			'user_space' => $userSpace,
			'id'         => $id
		] );

		$view = view( 'artlook::user_space.artist' );

		if ( ! is_null( $data ) ) {
			if ( is_array( $data ) ) {
				$data = current( $data );
			}

			if ( isset( $data->artistData ) && ! empty( $data->artistData ) ) {
				$allImages        = $this->getAllImages( $data->artistData );
				$data->all_images = $allImages;
			}

			$view->with( 'main_item', $data )
			     ->with( 'title', $data->full_name );
		}

        if ( ! is_null( $parent ) ) {
            $view->with( 'parent', $parent );

            if (is_array($parent->items) && !empty($parent->items)) {
                $data->works = $this->filterItemsByArtistId($parent->items, (int) $id);
            }
        }

        return $view;
	}

	/**
	 * Return a user
	 * artist item
	 *
	 * @param Request $request
	 * @param $userSpace
	 * @param $artistId
	 * @param $itemId
	 * @param $itemSlug
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */

	public function artistItem( Request $request, $userSpace, $artistId, $itemId, $itemSlug, $parameters = null ) {
		$userSpace  = $request->route()->hasParameter( 'user_space' ) ? $request->route()->parameter( 'user_space' ) : $userSpace;
		$artistId   = $request->route()->hasParameter( 'artist_id' ) ? $request->route()->parameter( 'artist_id' ) : $artistId;
		$itemId     = $request->route()->hasParameter( 'item_id' ) ? $request->route()->parameter( 'item_id' ) : $itemId;
		$itemSlug   = $request->route()->hasParameter( 'item_slug' ) ? $request->route()->parameter( 'item_slug' ) : $itemSlug;
		$parameters = $request->route()->hasParameter( 'parameters' ) ? $request->route()->parameter( 'parameters' ) : $parameters;

		$data = $this->getData( $request, 'artist', [
			'user_space' => $userSpace,
			'id'         => $artistId
		] );

		$parent = null;
		if ( ! is_null( $data ) ) {
			if ( is_array( $data ) ) {
				$parent = current( $data );
			} else {
				$parent = $data;
			}
		}

		return $this->item( $request, $userSpace, $itemId, $itemSlug, $parent, $parameters );
	}

	/**
	 * Return a user
	 * artist category
	 *
	 * @param $user
	 * @param $id
	 * @param $category
	 *
	 * @return string
	 */

	public function artistCategory( $user, $id, $category ) {
		return 'artist of ' . $user . ', artist id: ' . $id . ', category id: ' . $category . '.';
	}

	/**
	 * ToDo
	 *
	 * ZCL@20170823
	 *
	 * Añadir los controladores
	 * y las vistas necesari
	 * para gestionar los recursos
	 * de noticias.
	 */

	/**
	 * Return a user
	 * page
	 *
	 * @param $request
	 * @param $userSpace
	 * @param $id
	 *
	 * @return string
	 */

	public function page( Request $request, $userSpace, $id ) {
		$userSpace = $request->route()->hasParameter( 'user_space' ) ? $request->route()->parameter( 'user_space' ) : $userSpace;
		$id        = $request->route()->hasParameter( 'id' ) ? $request->route()->parameter( 'id' ) : $id;

		$data = $this->getData( $request, __FUNCTION__, [
			'user_space' => $userSpace,
			'id'         => $id,
		] );

        $view = view( 'artlook::user_space.page' )
            ->with('title', '');

		if ( ! is_null( $data ) ) {
			$mainItem = array_shift( $data );

			$allImages            = $this->getAllImages( $mainItem );
			$mainItem->all_images = $allImages;

			if ( ! empty( $allImages )
			     && ( ! isset( $mainItem->description ) || empty( $mainItem->description ) )
			) {
				$mainItem->grouping_classes = [ 'grouping-only-image' ];
				$mainItem->only_image_item  = true;
			} elseif ( empty( $allImages )
			           && ( isset( $mainItem->description ) && ! empty( $mainItem->description ) )
			) {
				$mainItem->grouping_classes = [ 'grouping-only-text' ];
				$mainItem->only_text_item   = true;
			}
			$view->with( 'main_item', $mainItem );

            if (isset($mainItem->title) && !empty($mainItem->title)) {
                $view->with('title', $mainItem->title);
            }
		}

		$settings = $request->attributes->get( 'settings' );

		$menu     = $settings->menu;

		foreach ( $menu as $menuItem ) {
			$menuItemPath = ( $request->attributes->get( 'use_user_type_for_the_route' ) ? $userSpace . '/' : '' ) . ( current( $menuItem ) !== 'home' ? ( current( $menuItem ) ) : '' );

			if ( $request->is( $menuItemPath ) ) {
				$view->with( 'title', key( $menuItem ) );
				break;
			}
		}

		return $view;
	}

	/**
	 * Return a user
	 * page item
	 *
	 * @param Request $request
	 * @param $userSpace
	 * @param $pageId
	 * @param $itemId
	 * @param $itemSlug
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */

	public function pageItem( Request $request, $userSpace, $pageId, $itemId, $itemSlug, $parameters = null ) {
		$userSpace  = $request->route()->hasParameter( 'user_space' ) ? $request->route()->parameter( 'user_space' ) : $userSpace;
		$pageId     = $request->route()->hasParameter( 'page_id' ) ? $request->route()->parameter( 'page_id' ) : $pageId;
		$itemId     = $request->route()->hasParameter( 'item_id' ) ? $request->route()->parameter( 'item_id' ) : $itemId;
		$itemSlug   = $request->route()->hasParameter( 'item_slug' ) ? $request->route()->parameter( 'item_slug' ) : $itemSlug;
		$parameters = $request->route()->hasParameter( 'parameters' ) ? $request->route()->parameter( 'parameters' ) : $parameters;

		$data = $this->getData( $request, 'page', [
			'user_space' => $userSpace,
			'id'         => $pageId
		] );

		$parent = null;
		if ( ! is_null( $data ) ) {
			if ( is_array( $data ) ) {
				$parent = current( $data );
			} else {
				$parent = $data;
			}
		}

		$viewHelper = new ViewHelper();
		$request->attributes->add( [
			'page_slug' => $viewHelper->getSlug( $parent->title )
		] );

		$request->request->add(['group' => $pageId]);

		return $this->item( $request, $userSpace, $itemId, $itemSlug, $parent, $parameters );
	}

    /**
     * Return a user
     * page artist.
     *
     * @param Request $request
     * @param $userSpace
     * @param $pageId
     * @param $pageSlug
     * @param $artistSlug
     * @param $artistId
     * @param null $parameters
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function pageArtist( Request $request, $userSpace, $pageId, $pageSlug, $artistSlug, $artistId, $parameters = null ) {
        $userSpace  = $request->route()->hasParameter( 'user_space' ) ? $request->route()->parameter( 'user_space' ) : $userSpace;
        $pageId     = $request->route()->hasParameter( 'page_id' ) ? $request->route()->parameter( 'page_id' ) : $pageId;
        $pageSlug   = $request->route()->hasParameter( 'page_slug' ) ? $request->route()->parameter( 'page_slug' ) : $pageSlug;
        $artistId     = $request->route()->hasParameter( 'artist_id' ) ? $request->route()->parameter( 'artist_id' ) : $artistId;
        $artistSlug   = $request->route()->hasParameter( 'artist_slug' ) ? $request->route()->parameter( 'artist_slug' ) : $artistSlug;
        $parameters = $request->route()->hasParameter( 'parameters' ) ? $request->route()->parameter( 'parameters' ) : $parameters;

        $data = $this->getData( $request, 'page', [
            'user_space' => $userSpace,
            'id'         => $pageId
        ] );

        $parent = null;
        if ( ! is_null( $data ) ) {
            if ( is_array( $data ) ) {
                $parent = current( $data );
            } else {
                $parent = $data;
            }
        }

        $viewHelper = new ViewHelper();
        $request->attributes->add( [
            'page_slug' => $viewHelper->getSlug( $parent->title )
        ] );

        return $this->artist( $request, $userSpace, $artistId, $artistSlug, $parent, $parameters );
    }

    public function pageArtistItem( Request $request, $userSpace, $pageId, $pageSlug, $artistSlug, $artistId, $itemSlug, $itemId, $parameters = null ) {
        $userSpace  = $request->route()->hasParameter( 'user_space' ) ? $request->route()->parameter( 'user_space' ) : $userSpace;
        $pageId     = $request->route()->hasParameter( 'page_id' ) ? $request->route()->parameter( 'page_id' ) : $pageId;
        $pageSlug   = $request->route()->hasParameter( 'page_slug' ) ? $request->route()->parameter( 'page_slug' ) : $pageSlug;
        $artistId     = $request->route()->hasParameter( 'artist_id' ) ? $request->route()->parameter( 'artist_id' ) : $artistId;
        $artistSlug   = $request->route()->hasParameter( 'artist_slug' ) ? $request->route()->parameter( 'artist_slug' ) : $artistSlug;
        $itemId     = $request->route()->hasParameter( 'item_id' ) ? $request->route()->parameter( 'item_id' ) : $itemId;
        $itemSlug   = $request->route()->hasParameter( 'item_slug' ) ? $request->route()->parameter( 'item_slug' ) : $itemSlug;
        $parameters = $request->route()->hasParameter( 'parameters' ) ? $request->route()->parameter( 'parameters' ) : $parameters;

        $data = $this->getData( $request, 'page', [
            'user_space' => $userSpace,
            'id'         => $pageId
        ] );

        $parent = null;
        if ( ! is_null( $data ) ) {
            if ( is_array( $data ) ) {
                $parent = current( $data );
            } else {
                $parent = $data;
            }
        }

        $viewHelper = new ViewHelper();
        $request->attributes->add( [
            'page_slug' => $viewHelper->getSlug( $parent->title )
        ] );

        return $this->item( $request, $userSpace, $itemId, $itemSlug, $parent, $parameters );
    }

	/**
	 * Return a user
	 * item
	 *
	 * @param $request
	 * @param $userSpace
	 * @param $id
	 * @param $slug
	 * @param $parent
	 * @param $nav
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */

	public function item( Request $request, $userSpace, $id, $slug, $parent = null, $parameters = null ) {

		$userSpace  = $request->route()->hasParameter( 'user_space' ) ? $request->route()->parameter( 'user_space' ) : $userSpace;
		$id         = $request->route()->hasParameter( 'id' ) ? $request->route()->parameter( 'id' ) : $id;
		$group      = $request->has( 'group' ) ? $request->input( 'group' ) : false;
		$slug       = $request->route()->hasParameter( 'slug' ) ? $request->route()->parameter( 'slug' ) : $slug;
		$parent     = $request->route()->hasParameter( 'parent' ) ? $request->route()->parameter( 'parent' ) : $parent;
		$parameters = $request->route()->hasParameter( 'parameters' ) ? $request->route()->parameter( 'parameters' ) : $parameters;

		$getDataParameters = [
			'user_space' => $userSpace,
			'id'         => $id,
			'slug'       => $slug,
            'group'      => $group
		];

		if ( ! empty( $parameters ) && is_array( $parameters ) ) {
			$getDataParameters = array_merge( $parameters, $getDataParameters );
		}

		$data = $this->getData( $request, __FUNCTION__, $getDataParameters );

		$view = view( 'artlook::user_space.item' );

		if ( ! is_null( $data ) ) {
			$allImages        = $this->getAllImages( $data );
			$data->all_images = $allImages;

			$view->with( 'main_item', $data );
			if ( isset( $data->title ) ) {
				$view->with( 'title', $data->title );
			}
		}

		if ( ! is_null( $parent ) ) {
			$view->with( 'parent', $parent );
		}

		return $view;
	}

    private function filterItemsByArtistId(array $items, int $artistId): array
    {
        return array_filter($items, function ($item) use ($artistId) {
            return $item->artistId === $artistId;
        });
    }
}
