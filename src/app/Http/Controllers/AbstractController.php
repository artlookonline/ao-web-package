<?php

namespace Artlook\Frontend\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

abstract class AbstractController extends BaseController
{
    /**
     * Protected methods
     */

    /**
     * @param Request $request
     * @param $endpoint
     * @return mixed
     */
    protected function getData(Request $request, $endpoint, $parameters = null)
    {
        $artlookEndpoint = 'artlook.api.endpoints.user.' . $endpoint;

        $apiBridgeHelper = app()->make('helper.api_bridge');

        $data = $apiBridgeHelper->getData($request, $artlookEndpoint, $parameters);

        return $data;
    }

    protected function getAllImages($item)
    {
        $allImages = [];

        if (isset($item->image) && !empty($item->image)) {
            $allImages[] = $item->image;
        }

        if (isset($item->profileimage) && !empty($item->profileimage)) {
            $allImages[] = $item->profileimage;
        }

        if (isset($item->images) && !empty($item->images)) {
            foreach ($item->images as $image) {
                $allImages[] = $image;
            }
        }

        if (isset($item->subimages) && !empty($item->subimages)) {
            foreach ($item->subimages as $image) {
                $allImages[] = $image;
            }
        }

        return $allImages;
    }
}
