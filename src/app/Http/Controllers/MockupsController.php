<?php

namespace Artlook\Frontend\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
//use Illuminate\Foundation\Auth\Access\AuthorizesResources;

class MockupsController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Return the user
     * homepage
     *
     * @param $template
     * @return string
     */

    public function template($template)
    {
        if (!view()->exists('artlook::mockups.'.$template)) {
            return '<pre>No existe ningún template que coincida con esta petición, por favor, asegúrate que existe el fichiero ./resources/views/mockups/'.$template.'.blade.php.<pre>';
        }

        return view('artlook::mockups.'.$template);
    }
}
