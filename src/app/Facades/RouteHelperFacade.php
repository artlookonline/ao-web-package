<?php

namespace Artlook\Frontend\Facades;

use Illuminate\Support\Facades\Facade;

class RouteHelperFacade extends Facade {

    protected static function getFacadeAccessor()
    {
        return 'helper.route';
    }
}